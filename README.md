
# the_movie_db
An app that people can see the now playing movies,search for a movie by title and see its details. 
Movie API [Powered by The Movie Database (TMDb)](https://www.themoviedb.org/documentation/api)

## Requirements for test
Create an emulator from API 19+ without Google Play APIs (TestButler doesn't work on Emulators with Google Play APIs) and start it before to run the commands below.

### Run test from Android Studio
Open the Android Studio's terminal and execute the command:
``./gradlew installButlerApk`` to install TestButler if you want to run the test from Android Studio's Run button.

### Run automatic tests
Open the project folder from terminal and execute the command:
``./gradlew runAllTests``. It will run all Unit Tests and Instrumented Tests.

### Run code coverage report
Open the project folder from terminal and execute the command:
``./gradlew jacocoTestReport``. It will run all Unit Tests, Instrumented Tests and will create an HTML file located at ` app/build/reports/jacoco/jacocoTestReport/html/index.html` with all info about the tests.

### Build and Run the application
Open the project folder from terminal and execute the command:
``./gradlew buildRun``. It will build, install and run the application inside the emulator.
