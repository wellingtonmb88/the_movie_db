package com.wellingtonmb88.themoviedb.presentation.ui.searchmovies

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.util.Pair
import android.support.v7.widget.*
import android.text.TextUtils
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView
import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView
import com.wellingtonmb88.themoviedb.*
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.presentation.NetworkHelper
import com.wellingtonmb88.themoviedb.presentation.base.BaseLifecycleActivity
import com.wellingtonmb88.themoviedb.presentation.ui.EXTRA_MOVIE_ITEM
import com.wellingtonmb88.themoviedb.presentation.ui.SEARCH_KEY
import com.wellingtonmb88.themoviedb.presentation.ui.moviedetails.MovieDetailsActivity
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_search_movies.*
import kotlinx.android.synthetic.main.toolbar_search_movies.*
import kotlinx.android.synthetic.main.view_empty_state.*
import kotlinx.android.synthetic.main.view_error_message.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SearchMoviesActivity : BaseLifecycleActivity() {

    @Inject
    lateinit var viewModelFactory: SearchMoviesViewModelFactory

    private val emptyStateView: View by lazy { empty_state_layout }
    private val errorMessageView: View by lazy { error_message_layout }
    private val loadNewestProgressBar: View by lazy { progress_bar_load_newest }
    private val loadMoreProgressBar: View by lazy { progress_bar_load_more }
    private val moviesRecyclerView: RecyclerView by lazy { movies_list_rcv }
    private val errorMessageTextView: TextView by lazy { error_message_text_view }
    private val tryAgainButton: Button by lazy { try_again_button }
    private val collapsingToolbar: CollapsingToolbarLayout by lazy { collapsing_toolbar }

    private lateinit var adapter: SearchMoviesAdapter

    private lateinit var viewModel: SearchMoviesViewModel

    private lateinit var layoutManager: LinearLayoutManager

    private var networkHelper: NetworkHelper? = null

    private var searchQuery = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_movies)
        setSupportActionBar(toolbar)

        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
        }

        if (networkHelper == null) {
            networkHelper = NetworkHelper.newInstance()
        }

        networkHelper?.let { addFragment(it, NetworkHelper.FRAGMENT_TAG) }

        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(SearchMoviesViewModel::class.java)

        layoutManager =
                if (isLandScapeOrientation()) {
                    GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
                } else {
                    LinearLayoutManager(this)
                }

        adapter = SearchMoviesAdapter { item, sharedElement ->
            onMovieClicked(item, sharedElement)
        }

        moviesRecyclerView.layoutManager = layoutManager
        moviesRecyclerView.adapter = adapter
        moviesRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        emptyStateView.visibility = if (adapter.dataSource.isEmpty()) View.VISIBLE else View.GONE

        observeLiveData()

        if (savedInstanceState != null
                && savedInstanceState.containsKey(SEARCH_KEY)) {
            searchQuery = savedInstanceState.getString(SEARCH_KEY)
        } else {
            lockCollapsingToolbar(true)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(SEARCH_KEY, searchQuery)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_movies_menu, menu)

        val myActionMenuItem = menu.findItem(R.id.action_search)
        val searchView = myActionMenuItem.actionView as SearchView

        searchView.setOnSearchClickListener { searchView.setQuery(searchQuery, false) }

        RxSearchView.queryTextChanges(searchView)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .debounce(500, TimeUnit.MILLISECONDS)
                .filter { characterSequence -> characterSequence.length > 1 && characterSequence != searchQuery }
                .subscribe { characterSequence ->
                    if (!TextUtils.isEmpty(characterSequence)) {
                        searchQuery = characterSequence.toString()

                        runOnUiThread {
                            checkInternetAvailable()
                            viewModel.searchMovies(searchQuery)
                        }
                    }
                }

        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        networkHelper = null
    }

    private fun onMovieClicked(movieModel: MovieModel, sharedElements: Array<Pair<View, String>>) {
        val intent = Intent(this, MovieDetailsActivity::class.java)
        intent.putExtra(EXTRA_MOVIE_ITEM, movieModel)
        startActivityWithSharedElements(intent, *sharedElements)
    }

    private fun observeLiveData() {
        viewModel.isLoadingLiveData.observe(this, Observer { shouldShowLoading ->
            shouldShowLoading?.let { showLoadingSearchMovies(shouldShowLoading) }
        })

        viewModel.showErrorSearchMoviesLiveData.observe(this, Observer { shouldShowError ->
            shouldShowError?.let { showErrorSearchMovies(shouldShowError) }
        })

        viewModel.showResolutionErrorLiveData.observe(this, Observer { resolutionViewState ->
            resolutionViewState?.let { showResolutionError(resolutionViewState) }
        })

        viewModel.moviesListLiveData.observe(this, Observer { moviesList ->
            moviesList?.let { onSearchMoviesResult(moviesList) }
        })

        viewModel.moviesListNexPageLiveData.observe(this, Observer { moviesList ->
            moviesList?.let { onNextPageResult(moviesList) }
        })

        viewModel.isLoadingNextPageLiveData.observe(this, Observer { shouldShowLoading ->
            shouldShowLoading?.let { showNextPageLoading(shouldShowLoading) }
        })

        viewModel.showErrorLoadingNexPageLiveData.observe(this, Observer<Boolean> { shouldShowError ->
            shouldShowError?.let { showNextPageError(shouldShowError) }
        })

        registerScrollEndListener()

        tryAgainButton.setOnClickListener {
            viewModel.searchMovies(searchQuery)
        }
    }

    private fun registerScrollEndListener() {
        RxRecyclerView.scrollStateChanges(moviesRecyclerView)
                .observeOn(AndroidSchedulers.mainThread())
                .filter { layoutManager.findLastCompletelyVisibleItemPosition() == adapter.dataSource.size - 1 }
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewModel.loadNexPage(searchQuery)
                }
    }

    private fun onSearchMoviesResult(moviesList: List<MovieModel>) {
        checkInternetAvailable()
        if (moviesList.isNotEmpty()) {
            lockCollapsingToolbar(false)
            moviesRecyclerView.visibility = View.VISIBLE
            emptyStateView.visibility = View.GONE
            adapter.dataSource = moviesList
            moviesRecyclerView.runLayoutAnimation(R.anim.layout_animation_slide_right)
        } else {
            if (adapter.dataSource.isEmpty()) {
                emptyStateView.visibility = View.VISIBLE
            }
            moviesRecyclerView.showSnackBar(getString(R.string.no_movie_found))
        }
    }

    private fun showLoadingSearchMovies(show: Boolean) {
        if (show) {
            emptyStateView.visibility = View.GONE
            errorMessageView.visibility = View.GONE
            loadNewestProgressBar.visibility = View.VISIBLE
        } else {
            loadNewestProgressBar.visibility = View.GONE
        }
    }

    private fun showErrorSearchMovies(show: Boolean) {
        if (show) {
            emptyStateView.visibility = View.GONE
            moviesRecyclerView.visibility = View.GONE
            errorMessageView.visibility = View.VISIBLE
        } else {
            errorMessageView.visibility = View.GONE
        }
    }

    private fun onNextPageResult(list: List<MovieModel>) {
        checkInternetAvailable()
        if (list.isNotEmpty()) {
            val tempList = mutableSetOf<MovieModel>()
            tempList.addAll(adapter.dataSource)
            tempList.addAll(list)
            adapter.dataSource = tempList.toList()
        } else {
            moviesRecyclerView.showSnackBar(getString(R.string.no_more_results))
        }
    }

    private fun showNextPageLoading(show: Boolean) {
        if (show) {
            loadMoreProgressBar.visibility = View.VISIBLE
        } else {
            loadMoreProgressBar.visibility = View.GONE
        }
    }

    private fun showNextPageError(show: Boolean) {
        if (show) {
            if (checkInternetAvailable()) {
                moviesRecyclerView.showSnackBar(getString(R.string.error_loading_more))
            }
        }
    }

    private fun showResolutionError(resolutionViewState: ResolutionByCase.ResolutionViewState) {
        tryAgainButton.visibility = View.GONE
        when (resolutionViewState) {
            is ResolutionByCase.ResolutionViewState.NotFound -> {
                Timber.d("Show Not Found Message")
                errorMessageTextView.text = getString(R.string.requested_server_not_found)
            }
            is ResolutionByCase.ResolutionViewState.InternalServerError -> {
                Timber.d("Show InternalServerError Message")
                errorMessageTextView.text = getString(R.string.unexpected_error_message)
            }
            is ResolutionByCase.ResolutionViewState.ServiceUnavailable -> {
                Timber.d("Show ServiceUnavailable Message")
                errorMessageTextView.text = getString(R.string.server_unavailable_message)
                tryAgainButton.visibility = View.VISIBLE
            }
            is ResolutionByCase.ResolutionViewState.InternetUnavailable -> {
                Timber.d("Show NoInternetConnection Message")
                checkInternetAvailable()
                emptyStateView.visibility = if (adapter.dataSource.isEmpty()) View.VISIBLE else View.GONE
            }
        }
    }

    private fun lockCollapsingToolbar(locked: Boolean) {
        val params = collapsingToolbar.layoutParams as AppBarLayout.LayoutParams
        if (locked) {
            params.scrollFlags = 0
        } else {
            params.scrollFlags =
                    AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout
                            .LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout
                            .LayoutParams.SCROLL_FLAG_ENTER_ALWAYS_COLLAPSED
        }
        collapsingToolbar.requestLayout()
    }
}
