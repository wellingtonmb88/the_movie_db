package com.wellingtonmb88.themoviedb.presentation.resolution

import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase.ResolutionViewState.*
import javax.inject.Inject

class UIResolution
@Inject
constructor() : ResolutionByCase() {

    override fun onInternetUnavailable(): ResolutionViewState = InternetUnavailable()

    override fun onNotFound(): ResolutionViewState = NotFound()

    override fun onServiceUnavailable(): ResolutionViewState = ServiceUnavailable()

    override fun onInternalServerError(): ResolutionViewState = InternalServerError()
}