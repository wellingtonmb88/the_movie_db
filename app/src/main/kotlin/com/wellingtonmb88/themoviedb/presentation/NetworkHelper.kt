package com.wellingtonmb88.themoviedb.presentation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import com.wellingtonmb88.themoviedb.R
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.contentView


class NetworkHelper private constructor() : Fragment() {

    companion object {
        const val CHECK_INTERNET_ACTION = "NetworkHelper_INTENT_FILTER_CHECK_INTERNET_ACTION"
        const val NETWORK_STATE_EXTRA = "NetworkHelper_NETWORK_STATE_EXTRA"
        const val FRAGMENT_TAG = "NetworkHelper_FRAGMENT_TAG"

        fun newInstance(): NetworkHelper = NetworkHelper()
    }

    private val JOB_SERVICE_TAG = "NETWORK_STATE_CHECKER_TAG"

    private var persistentSnackbar: Snackbar? = null

    private var broadcastReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        persistentSnackbar = Snackbar.make(activity.contentView!!,
                R.string.no_internet_connection, Snackbar.LENGTH_INDEFINITE)
        persistentSnackbar?.view?.backgroundColor = ContextCompat.getColor(context, R.color.colorAccent)
    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(context).registerReceiver(getReceiver(), IntentFilter(CHECK_INTERNET_ACTION))
    }

    override fun onPause() {
        super.onPause()
        LocalBroadcastManager.getInstance(context).unregisterReceiver(getReceiver())
    }

    override fun onDestroy() {
        super.onDestroy()
        broadcastReceiver = null
    }

    private fun getReceiver(): BroadcastReceiver {

        if (broadcastReceiver == null) {
            broadcastReceiver = object : BroadcastReceiver() {

                override fun onReceive(context: Context, intent: Intent) {
                    if (intent.action == CHECK_INTERNET_ACTION) {
                        val isNetworkConnected = intent.getBooleanExtra(NETWORK_STATE_EXTRA, false)
                        if (isNetworkConnected) {
                            hidePersistentSnackBar()
                        } else {
                            showNetworkErrorMessage()
                        }
                    }
                }
            }
        }
        return broadcastReceiver!!
    }

    private fun showNetworkErrorMessage() {
        if (persistentSnackbar?.isShown == false) {
            persistentSnackbar?.show()
        }
    }

    private fun hidePersistentSnackBar() {
        persistentSnackbar?.dismiss()
    }
}
