package com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesViewState
import com.wellingtonmb88.themoviedb.presentation.resolution.UIResolution
import retrofit2.HttpException
import java.io.IOException


class NowPlayingMoviesViewModel(interactor: NowPlayingMoviesInteractor,
                                private val resolution: UIResolution) : ViewModel() {

    companion object {
        const val AVERAGE = 5.0
    }

    val isLoadingLiveData = MediatorLiveData<Boolean>()

    val showErrorNowPlayingMoviesLiveData = MediatorLiveData<Boolean>()

    private val averageLiveData = MutableLiveData<Double>()

    private val resultNowPlayingMoviesLiveData = NowPlayingMoviesLiveData(interactor).apply {
        this.addSource(averageLiveData) { it?.let { this.average = it } }
    }

    val moviesListLiveData = MediatorLiveData<List<MovieModel>>().apply {
        this.addSource(resultNowPlayingMoviesLiveData) { viewState ->
            viewState?.let { renderNowPlayingMovies(it) }
        }
    }

    val showResolutionErrorLiveData = MediatorLiveData<ResolutionByCase.ResolutionViewState>().apply {
        this.addSource(resultNowPlayingMoviesLiveData) { viewState ->
            viewState?.let {
                if (it is NowPlayingMoviesViewState.Error) {
                    onError(it.error)
                }
            }
        }
    }

    val isLoadingNextPageLiveData = MediatorLiveData<Boolean>()

    val showErrorLoadingNexPageLiveData = MediatorLiveData<Boolean>()

    private val averageLoadNextPageLiveData = MutableLiveData<Double>()

    private val resultLoadNextPageRepoLiveData = NowPlayingMoviesLoadNextPageLiveData(interactor).apply {
        this.addSource(averageLoadNextPageLiveData) { it?.let { this.average = it } }
    }

    val moviesListNexPageLiveData = MediatorLiveData<List<MovieModel>>().apply {
        this.addSource(resultLoadNextPageRepoLiveData) { viewState ->
            viewState?.let { renderNextPage(it) }
        }
    }

    fun loadNowPlayingMovies() {
        averageLiveData.value = AVERAGE
    }

    fun loadNexPage() {
        averageLoadNextPageLiveData.value = AVERAGE
    }

    private fun renderNowPlayingMovies(viewState: NowPlayingMoviesViewState) {
        when (viewState) {
            is NowPlayingMoviesViewState.Loading -> {
                showErrorNowPlayingMoviesLiveData.value = false
                isLoadingLiveData.value = true
            }
            is NowPlayingMoviesViewState.EmptyResult -> {
                showErrorNowPlayingMoviesLiveData.value = false
                isLoadingLiveData.value = false
                moviesListLiveData.value = listOf()
            }
            is NowPlayingMoviesViewState.Error -> {
                onError(viewState.error)
            }
            is NowPlayingMoviesViewState.Result -> {
                showErrorNowPlayingMoviesLiveData.value = false
                isLoadingLiveData.value = false
                moviesListLiveData.value = viewState.result
            }
        }
    }

    private fun renderNextPage(viewState: NowPlayingMoviesViewState) {

        when (viewState) {
            is NowPlayingMoviesViewState.EmptyResultNextPage -> {
                isLoadingNextPageLiveData.value = false
                showErrorLoadingNexPageLiveData.value = false
                moviesListNexPageLiveData.value = listOf()
            }
            is NowPlayingMoviesViewState.LoadingNextPage -> {
                isLoadingNextPageLiveData.value = true
                showErrorLoadingNexPageLiveData.value = false
            }
            is NowPlayingMoviesViewState.ErrorNexPage -> {
                isLoadingNextPageLiveData.value = false
                showErrorLoadingNexPageLiveData.value = true
            }
            is NowPlayingMoviesViewState.ResultNextPage -> {
                isLoadingNextPageLiveData.value = false
                showErrorLoadingNexPageLiveData.value = false
                moviesListNexPageLiveData.value = viewState.result
            }
        }
    }

    private fun onError(throwable: Throwable) {
        isLoadingLiveData.value = false
        when (throwable) {
            is HttpException -> {
                showErrorNowPlayingMoviesLiveData.value = true
                val resolutionViewState = resolution.onHttpException(throwable.code())
                showResolutionErrorLiveData.value = resolutionViewState
            }
            is IOException -> {
                showErrorNowPlayingMoviesLiveData.value = false
                val resolutionViewState = resolution.onInternetUnavailable()
                showResolutionErrorLiveData.value = resolutionViewState
            }
            else -> throwable.apply {
                showErrorNowPlayingMoviesLiveData.value = true
            }
        }
    }

}
