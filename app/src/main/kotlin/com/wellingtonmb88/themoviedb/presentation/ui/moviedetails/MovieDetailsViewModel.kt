package com.wellingtonmb88.themoviedb.presentation.ui.moviedetails

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.Intent
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.presentation.ui.EXTRA_MOVIE_ITEM
import timber.log.Timber

class MovieDetailsViewModel : ViewModel() {

    private val intentLiveData = MutableLiveData<Intent>()

    val showErrorLiveData = MediatorLiveData<Boolean>()

    val movieLiveData = MediatorLiveData<MovieModel>().apply {
        this.addSource(intentLiveData) { intent ->
            if (intent?.hasExtra(EXTRA_MOVIE_ITEM) == true) {
                onLoadExtra(intent.getParcelableExtra(EXTRA_MOVIE_ITEM))
            } else {
                onErrorLoadingExtra()
            }
        }
    }

    fun loadMovie(intent: Intent) {
        Timber.d("Calling loadMovie Method!")
        intentLiveData.value = intent
    }

    private fun onLoadExtra(movie: MovieModel) {
        Timber.d("Calling onLoadExtra Method!")
        movieLiveData.value = movie
    }

    private fun onErrorLoadingExtra() {
        Timber.d("Calling onErrorLoadingExtra Method!")
        showErrorLiveData.value = true
    }
}