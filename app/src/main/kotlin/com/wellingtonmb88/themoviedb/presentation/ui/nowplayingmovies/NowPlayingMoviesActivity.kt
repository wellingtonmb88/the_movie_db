package com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.util.Pair
import android.support.v7.widget.*
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.jakewharton.rxbinding2.support.v7.widget.RxRecyclerView
import com.wellingtonmb88.themoviedb.*
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.presentation.NetworkHelper
import com.wellingtonmb88.themoviedb.presentation.base.BaseLifecycleActivity
import com.wellingtonmb88.themoviedb.presentation.ui.EXTRA_MOVIE_ITEM
import com.wellingtonmb88.themoviedb.presentation.ui.moviedetails.MovieDetailsActivity
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesActivity
import dagger.android.AndroidInjection
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_now_playing_movies.*
import kotlinx.android.synthetic.main.toolbar_main.*
import kotlinx.android.synthetic.main.view_empty_state.*
import kotlinx.android.synthetic.main.view_error_message.*
import timber.log.Timber
import javax.inject.Inject


class NowPlayingMoviesActivity : BaseLifecycleActivity() {

    @Inject
    lateinit var viewModelFactory: NowPlayingMoviesViewModelFactory

    private val emptyStateView: View by lazy { empty_state_layout }
    private val errorMessageView: View by lazy { error_message_layout }
    private val loadNewestProgressBar: View by lazy { progress_bar_load_newest }
    private val loadMoreProgressBar: View by lazy { progress_bar_load_more }
    private val moviesRecyclerView: RecyclerView by lazy { movies_list_rcv }
    private val errorMessageTextView: TextView by lazy { error_message_text_view }
    private val tryAgainButton: Button by lazy { try_again_button }
    private val toolbarLayout: Toolbar by lazy { toolbar }

    private lateinit var adapter: NowPlayingMoviesAdapter

    private lateinit var viewModel: NowPlayingMoviesViewModel

    private lateinit var layoutManager: LinearLayoutManager

    private var networkHelper: NetworkHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_now_playing_movies)
        setSupportActionBar(toolbarLayout)

        if (networkHelper == null) {
            networkHelper = NetworkHelper.newInstance()
        }

        networkHelper?.let { addFragment(it, NetworkHelper.FRAGMENT_TAG) }

        viewModel = ViewModelProviders
                .of(this, viewModelFactory)
                .get(NowPlayingMoviesViewModel::class.java)

        layoutManager =
                if (isLandScapeOrientation()) {
                    GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
                } else {
                    LinearLayoutManager(this)
                }

        adapter = NowPlayingMoviesAdapter { item, sharedElement ->
            onMovieClicked(item, sharedElement)
        }

        moviesRecyclerView.layoutManager = layoutManager
        moviesRecyclerView.adapter = adapter
        moviesRecyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        emptyStateView.visibility = if (adapter.dataSource.isEmpty()) View.VISIBLE else View.GONE

        observeLiveData()

        viewModel.loadNowPlayingMovies()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
            when (item.itemId) {
                R.id.action_search -> {
                    startActivity(Intent(this, SearchMoviesActivity::class.java))
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    override fun onDestroy() {
        super.onDestroy()
        networkHelper = null
    }

    private fun onMovieClicked(movieModel: MovieModel, sharedElements: Array<Pair<View, String>>) {
        val intent = Intent(this, MovieDetailsActivity::class.java)
        intent.putExtra(EXTRA_MOVIE_ITEM, movieModel)
        startActivityWithSharedElements(intent, *sharedElements)
    }

    private fun observeLiveData() {
        viewModel.isLoadingLiveData.observe(this, Observer { shouldShowLoading ->
            shouldShowLoading?.let { showLoadingNowPlayingMovies(shouldShowLoading) }
        })

        viewModel.showErrorNowPlayingMoviesLiveData.observe(this, Observer { shouldShowError ->
            shouldShowError?.let { showErrorNowPlayingMovies(shouldShowError) }
        })

        viewModel.showResolutionErrorLiveData.observe(this, Observer { resolutionViewState ->
            resolutionViewState?.let { showResolutionError(resolutionViewState) }
        })

        viewModel.moviesListLiveData.observe(this, Observer { moviesList ->
            moviesList?.let { onNowPlayingMoviesResult(moviesList) }
        })

        viewModel.moviesListNexPageLiveData.observe(this, Observer { moviesList ->
            moviesList?.let { onNextPageResult(moviesList) }
        })

        viewModel.isLoadingNextPageLiveData.observe(this, Observer { shouldShowLoading ->
            shouldShowLoading?.let { showNextPageLoading(shouldShowLoading) }
        })

        viewModel.showErrorLoadingNexPageLiveData.observe(this, Observer<Boolean> { shouldShowError ->
            shouldShowError?.let { showNextPageError(shouldShowError) }
        })

        registerScrollEndListener()

        tryAgainButton.setOnClickListener {
            viewModel.loadNowPlayingMovies()
        }
    }

    private fun registerScrollEndListener() {
        RxRecyclerView.scrollStateChanges(moviesRecyclerView)
                .observeOn(AndroidSchedulers.mainThread())
                .filter { layoutManager.findLastCompletelyVisibleItemPosition() == adapter.dataSource.size - 1 }
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewModel.loadNexPage()
                }
    }

    private fun onNowPlayingMoviesResult(moviesList: List<MovieModel>) {
        checkInternetAvailable()
        if (moviesList.isNotEmpty()) {
            moviesRecyclerView.visibility = View.VISIBLE
            emptyStateView.visibility = View.GONE
            adapter.dataSource = moviesList
            moviesRecyclerView.runLayoutAnimation(R.anim.layout_animation_slide_right)
        } else {
            if (adapter.dataSource.isEmpty()) {
                emptyStateView.visibility = View.VISIBLE
            }
            moviesRecyclerView.showSnackBar(getString(R.string.no_movie_found))
        }
    }

    private fun showLoadingNowPlayingMovies(show: Boolean) {
        if (show) {
            emptyStateView.visibility = View.GONE
            errorMessageView.visibility = View.GONE
            loadNewestProgressBar.visibility = View.VISIBLE
        } else {
            loadNewestProgressBar.visibility = View.GONE
        }
    }

    private fun showErrorNowPlayingMovies(show: Boolean) {
        if (show) {
            emptyStateView.visibility = View.GONE
            moviesRecyclerView.visibility = View.GONE
            errorMessageView.visibility = View.VISIBLE
        } else {
            errorMessageView.visibility = View.GONE
        }
    }

    private fun onNextPageResult(list: List<MovieModel>) {
        checkInternetAvailable()
        if (list.isNotEmpty()) {
            val tempList = mutableSetOf<MovieModel>()
            tempList.addAll(adapter.dataSource)
            tempList.addAll(list)
            adapter.dataSource = tempList.toList()
        } else {
            moviesRecyclerView.showSnackBar(getString(R.string.no_more_results))
        }
    }

    private fun showNextPageLoading(show: Boolean) {
        if (show) {
            loadMoreProgressBar.visibility = View.VISIBLE
        } else {
            loadMoreProgressBar.visibility = View.GONE
        }
    }

    private fun showNextPageError(show: Boolean) {
        if (show) {
            if (checkInternetAvailable()) {
                moviesRecyclerView.showSnackBar(getString(R.string.error_loading_more))
            }
        }
    }

    private fun showResolutionError(resolutionViewState: ResolutionByCase.ResolutionViewState) {
        tryAgainButton.visibility = View.GONE
        when (resolutionViewState) {
            is ResolutionByCase.ResolutionViewState.NotFound -> {
                Timber.d("Show Not Found Message")
                errorMessageTextView.text = getString(R.string.requested_server_not_found)
            }
            is ResolutionByCase.ResolutionViewState.InternalServerError -> {
                Timber.d("Show InternalServerError Message")
                errorMessageTextView.text = getString(R.string.unexpected_error_message)
            }
            is ResolutionByCase.ResolutionViewState.ServiceUnavailable -> {
                Timber.d("Show ServiceUnavailable Message")
                errorMessageTextView.text = getString(R.string.server_unavailable_message)
                tryAgainButton.visibility = View.VISIBLE
            }
            is ResolutionByCase.ResolutionViewState.InternetUnavailable -> {
                Timber.d("Show NoInternetConnection Message")
                checkInternetAvailable()
                emptyStateView.visibility = if (adapter.dataSource.isEmpty()) View.VISIBLE else View.GONE
            }
        }
    }
}
