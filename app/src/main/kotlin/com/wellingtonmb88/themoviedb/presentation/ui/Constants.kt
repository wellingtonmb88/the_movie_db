package com.wellingtonmb88.themoviedb.presentation.ui

const val MOVIE_POSTER_TRANSITION_KEY = "MOVIE_POSTER_TRANSITION_KEY"
const val EXTRA_MOVIE_ITEM = "EXTRA_MOVIE_ITEM"
const val SEARCH_KEY = "SearchMoviesActivity_EXTRA_SEARCH_KEY"