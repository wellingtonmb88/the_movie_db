package com.wellingtonmb88.themoviedb.presentation.base

import android.support.v4.util.Pair
import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseViewHolder<D>(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    abstract fun onBind(model: D, listener: (item: D, sharedElements: Array<Pair<View, String>>) -> Unit)
}