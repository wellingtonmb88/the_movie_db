package com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider


class NowPlayingMoviesViewModelFactory(private val viewModel: NowPlayingMoviesViewModel) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>?): T {
        if (modelClass?.isAssignableFrom(NowPlayingMoviesViewModel::class.java) == true) {
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}