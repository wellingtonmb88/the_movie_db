package com.wellingtonmb88.themoviedb.presentation.ui.moviedetails

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.transition.ChangeBounds
import android.view.MenuItem
import android.view.View
import android.view.animation.AnticipateOvershootInterpolator
import android.widget.ImageView
import android.widget.TextView
import com.wellingtonmb88.themoviedb.*
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.presentation.base.BaseLifecycleActivity
import com.wellingtonmb88.themoviedb.presentation.ui.MOVIE_POSTER_TRANSITION_KEY
import kotlinx.android.synthetic.main.activity_movie_details.*
import kotlinx.android.synthetic.main.content_movie_details.*
import kotlinx.android.synthetic.main.view_error_message.*


class MovieDetailsActivity : BaseLifecycleActivity() {

    private val TRANSITION_DURATION = 1000L

    private val moviePosterImageView: ImageView by lazy { movie_poster }
    private val collapsingToolbarLayout: CollapsingToolbarLayout by lazy { toolbar_layout }
    private val movieLayoutView: View by lazy { movie_details_layout }
    private val movieOverviewTextView: TextView by lazy { movie_overview }
    private val movieGenresTextView: TextView by lazy { movie_genres }
    private val movieReleaseDateTextView: TextView by lazy { movie_releaseDate }
    private val movieAdultTextView: TextView by lazy { movie_adult }
    private val movieVoteAverageTextView: TextView by lazy { movie_vote_average }
    private val errorMessageLayoutView: View by lazy { error_message_layout }

    private lateinit var viewModel: MovieDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupTransition()
        setContentView(R.layout.activity_movie_details)
        supportPostponeEnterTransition()
        setSupportActionBar(toolbar)

        errorMessageLayoutView.visibility = View.GONE

        collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT)

        moviePosterImageView.setupTransitionName(MOVIE_POSTER_TRANSITION_KEY)

        viewModel = ViewModelProviders.of(this).get(MovieDetailsViewModel::class.java)

        observeLiveData()

        viewModel.loadMovie(intent)
    }

    override fun onBackPressed() {
        supportFinishAfterTransition()
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> {
                    supportFinishAfterTransition()
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }

    private fun setupTransition() {
        window.setupSharedElementEnterTransition {
            val changeBounds = ChangeBounds()
            changeBounds.interpolator = AnticipateOvershootInterpolator()
            changeBounds.duration = TRANSITION_DURATION
            changeBounds
        }
    }

    private fun observeLiveData() {
        viewModel.movieLiveData.observe(this, Observer { movie ->
            movie?.let {
                setupToolbar(it.title)
                loadMoviePoster(it.posterPath)
                loadContent(it)
            }
        })

        viewModel.showErrorLiveData.observe(this, Observer { shouldShowError ->
            shouldShowError?.let {
                errorMessageLayoutView.visibility = View.VISIBLE
            }
        })
    }

    private fun setupToolbar(movieTitle: String) {
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(true)
            title = movieTitle
        }
    }

    private fun loadMoviePoster(posterPath: String) {
        moviePosterImageView.loadImage(posterPath,
                onResourceReady = { supportStartPostponedEnterTransition() },
                onLoadFailed = { supportStartPostponedEnterTransition() })
    }

    private fun loadContent(movie: MovieModel) {
        movieLayoutView.visibility = View.VISIBLE
        movieOverviewTextView.text = movie.overview
        movieReleaseDateTextView.text = dateFormatter(movie.releaseDate)
        movieVoteAverageTextView.text = movie.voteAverage.toString()

        val styleSpanBold = StyleSpan(Typeface.BOLD)
        val textSize = resources.getDimensionPixelSize(R.dimen.text_size_16sp)
        val absoluteSizeSpan = AbsoluteSizeSpan(textSize)

        if (movie.genres.isNotEmpty()) {
            val movieGenresSpannableString = SpannableStringBuilder("Genres: ${formatMovieGenresText(movie.genres)}")
            movieGenresSpannableString.setSpan(styleSpanBold, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            movieGenresSpannableString.setSpan(absoluteSizeSpan, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            movieGenresTextView.text = movieGenresSpannableString
        }

        val movieAdultSpannableString = SpannableStringBuilder("Adult: ${formatMovieAdultText(movie.adult)}")
        movieAdultSpannableString.setSpan(styleSpanBold, 0, 5, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        movieAdultSpannableString.setSpan(absoluteSizeSpan, 0, 5, Spannable.SPAN_INCLUSIVE_INCLUSIVE)

        movieAdultTextView.text = movieAdultSpannableString
    }
}
