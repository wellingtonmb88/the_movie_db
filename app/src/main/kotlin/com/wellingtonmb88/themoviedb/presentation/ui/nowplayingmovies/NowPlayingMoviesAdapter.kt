package com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies

import android.support.v4.util.Pair
import android.view.View
import com.wellingtonmb88.themoviedb.*
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.presentation.base.BaseAdapter
import com.wellingtonmb88.themoviedb.presentation.base.BaseViewHolder
import com.wellingtonmb88.themoviedb.presentation.ui.MOVIE_POSTER_TRANSITION_KEY
import kotlinx.android.synthetic.main.view_movie_item.view.*


class NowPlayingMoviesAdapter(clickItemListener: (item: MovieModel, sharedElements: Array<Pair<View, String>>) -> Unit) :
        BaseAdapter<MovieModel, NowPlayingMoviesAdapter.NowPlayingMoviesViewHolder>(clickItemListener) {

    override fun getItemViewId() = R.layout.view_movie_item

    override fun instantiateViewHolder(view: View?): NowPlayingMoviesViewHolder =
            NowPlayingMoviesViewHolder(view)

    class NowPlayingMoviesViewHolder(itemView: View?) : BaseViewHolder<MovieModel>(itemView) {

        override fun onBind(model: MovieModel, listener: (item: MovieModel, sharedElements: Array<Pair<View, String>>) -> Unit) =
                with(itemView) {
                    title.text = model.title
                    release_date.text = dateFormatter(model.releaseDate)
                    vote_average.text = model.voteAverage.toString()
                    genre.text = formatMovieGenresText(model.genres)

                    movie_poster.loadImage(model.posterPath)

                    movie_poster.setupTransitionName(MOVIE_POSTER_TRANSITION_KEY)

                    val posterPair = Pair.create(movie_poster as View, MOVIE_POSTER_TRANSITION_KEY)
                    val arrayOfPairs = arrayOf<Pair<View, String>>(posterPair)

                    setOnClickListener { listener(model, arrayOfPairs) }
                }
    }
}