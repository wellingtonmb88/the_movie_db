package com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies

import android.arch.lifecycle.MediatorLiveData
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class NowPlayingMoviesLiveData(private val interactor: NowPlayingMoviesInteractor) : MediatorLiveData<NowPlayingMoviesViewState>() {

    private var disposable: Disposable? = null

    var average: Double = 0.0
        set(value) {
            disposable = interactor.loadListByAverageHigherThan(value)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext { viewState ->
                        this@NowPlayingMoviesLiveData.value = viewState
                    }.subscribe()
        }

    override fun onInactive() {
        super.onInactive()
        if (disposable?.isDisposed?.not() == true) {
            disposable?.dispose()
        }
    }
}