package com.wellingtonmb88.themoviedb.presentation.base

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.LifecycleRegistryOwner
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

abstract class BaseLifecycleActivity : AppCompatActivity(), LifecycleRegistryOwner {

    private lateinit var lifecycleRegistry: LifecycleRegistry

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleRegistry = LifecycleRegistry(this)
    }

    override fun getLifecycle() = lifecycleRegistry

    protected fun addFragment(fragment: Fragment, fragmentTag: String) {

        val fragmentByTag = getFragmentByTag(fragmentTag)

        if(fragmentByTag == null) {
            supportFragmentManager
                    .beginTransaction()
                    .add(fragment, fragmentTag)
                    .commit()
        }
    }

    private fun getFragmentByTag(fragmentTag: String): Fragment? =
            supportFragmentManager.findFragmentByTag(fragmentTag)
}
