package com.wellingtonmb88.themoviedb.presentation.ui.searchmovies

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesViewState
import com.wellingtonmb88.themoviedb.presentation.resolution.UIResolution
import retrofit2.HttpException
import java.io.IOException


class SearchMoviesViewModel(interactor: SearchMoviesInteractor,
                            private val resolution: UIResolution) : ViewModel() {

    val isLoadingLiveData = MediatorLiveData<Boolean>()

    val showErrorSearchMoviesLiveData = MediatorLiveData<Boolean>()

    private val queryLiveData = MutableLiveData<String>()

    private val resultSearchMoviesLiveData = SearchMoviesLiveData(interactor).apply {
        this.addSource(queryLiveData) { it?.let { this.query = it } }
    }

    val moviesListLiveData = MediatorLiveData<List<MovieModel>>().apply {
        this.addSource(resultSearchMoviesLiveData) { viewState ->
            viewState?.let { renderSearchMovies(it) }
        }
    }

    val showResolutionErrorLiveData = MediatorLiveData<ResolutionByCase.ResolutionViewState>().apply {
        this.addSource(resultSearchMoviesLiveData) { viewState ->
            viewState?.let {
                if (it is SearchMoviesViewState.Error) {
                    onError(it.error)
                }
            }
        }
    }

    val isLoadingNextPageLiveData = MediatorLiveData<Boolean>()

    val showErrorLoadingNexPageLiveData = MediatorLiveData<Boolean>()

    private val queryLoadNextPageLiveData = MutableLiveData<String>()

    private val resultLoadNextPageRepoLiveData = SearchMoviesLoadNextPageLiveData(interactor).apply {
        this.addSource(queryLoadNextPageLiveData) { it?.let { this.query = it } }
    }

    val moviesListNexPageLiveData = MediatorLiveData<List<MovieModel>>().apply {
        this.addSource(resultLoadNextPageRepoLiveData) { viewState ->
            viewState?.let { renderNextPage(it) }
        }
    }

    fun searchMovies(query: String) {
        queryLiveData.value = query
    }

    fun loadNexPage(query: String) {
        queryLoadNextPageLiveData.value = query
    }

    private fun renderSearchMovies(viewState: SearchMoviesViewState) {
        when (viewState) {
            is SearchMoviesViewState.Loading -> {
                showErrorSearchMoviesLiveData.value = false
                isLoadingLiveData.value = true
            }
            is SearchMoviesViewState.EmptyResult -> {
                showErrorSearchMoviesLiveData.value = false
                isLoadingLiveData.value = false
                moviesListLiveData.value = listOf()
            }
            is SearchMoviesViewState.Error -> {
                onError(viewState.error)
            }
            is SearchMoviesViewState.Result -> {
                showErrorSearchMoviesLiveData.value = false
                isLoadingLiveData.value = false
                moviesListLiveData.value = viewState.result
            }
        }
    }

    private fun renderNextPage(viewState: SearchMoviesViewState) {

        when (viewState) {
            is SearchMoviesViewState.EmptyResultNextPage -> {
                isLoadingNextPageLiveData.value = false
                showErrorLoadingNexPageLiveData.value = false
                moviesListNexPageLiveData.value = listOf()
            }
            is SearchMoviesViewState.LoadingNextPage -> {
                isLoadingNextPageLiveData.value = true
                showErrorLoadingNexPageLiveData.value = false
            }
            is SearchMoviesViewState.ErrorNexPage -> {
                isLoadingNextPageLiveData.value = false
                showErrorLoadingNexPageLiveData.value = true
            }
            is SearchMoviesViewState.ResultNextPage -> {
                isLoadingNextPageLiveData.value = false
                showErrorLoadingNexPageLiveData.value = false
                moviesListNexPageLiveData.value = viewState.result
            }
        }
    }

    private fun onError(throwable: Throwable) {
        isLoadingLiveData.value = false
        when (throwable) {
            is HttpException -> {
                showErrorSearchMoviesLiveData.value = true
                val resolutionViewState = resolution.onHttpException(throwable.code())
                showResolutionErrorLiveData.value = resolutionViewState
            }
            is IOException -> {
                showErrorSearchMoviesLiveData.value = false
                val resolutionViewState = resolution.onInternetUnavailable()
                showResolutionErrorLiveData.value = resolutionViewState
            }
            else -> throwable.apply {
                showErrorSearchMoviesLiveData.value = true
            }
        }
    }

}
