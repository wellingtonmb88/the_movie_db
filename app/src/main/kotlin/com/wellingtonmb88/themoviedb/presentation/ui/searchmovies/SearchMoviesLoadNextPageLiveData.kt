package com.wellingtonmb88.themoviedb.presentation.ui.searchmovies

import android.arch.lifecycle.MediatorLiveData
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesViewState
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class SearchMoviesLoadNextPageLiveData(private val interactor: SearchMoviesInteractor) : MediatorLiveData<SearchMoviesViewState>() {

    private var disposable: Disposable? = null

    var query: String = ""
        set(value) {
            disposable = interactor.loadNextPage(value)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext { viewState ->
                        this@SearchMoviesLoadNextPageLiveData.value = viewState
                    }.subscribe()
        }

    override fun onInactive() {
        super.onInactive()
        if (disposable?.isDisposed?.not() == true) {
            disposable?.dispose()
        }
    }

}