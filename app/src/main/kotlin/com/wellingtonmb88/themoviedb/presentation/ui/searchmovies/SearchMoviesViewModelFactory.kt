package com.wellingtonmb88.themoviedb.presentation.ui.searchmovies

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider


class SearchMoviesViewModelFactory(private val viewModel: SearchMoviesViewModel) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>?): T {
        if (modelClass?.isAssignableFrom(SearchMoviesViewModel::class.java) == true) {
            return viewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}