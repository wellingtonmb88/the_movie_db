package com.wellingtonmb88.themoviedb.presentation.base

import android.support.v4.util.Pair
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.wellingtonmb88.themoviedb.inflate
import kotlin.properties.Delegates


abstract class BaseAdapter<D, VH : BaseViewHolder<D>>(private val listener: (item: D, sharedElements: Array<Pair<View, String>>) -> Unit) :
        RecyclerView.Adapter<VH>(), AutoUpdatableAdapter {

    var dataSource: List<D> by Delegates.observable(emptyList()) { _, old, new ->
        autoNotify(old, new) { o, n -> o == n }
    }

    private var lastPosition = RecyclerView.NO_POSITION

    private var animationSlideLeft = true

    override fun getItemCount() = dataSource.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): VH =
            instantiateViewHolder(parent?.inflate(getItemViewId()))

    abstract fun getItemViewId(): Int

    abstract fun instantiateViewHolder(view: View?): VH

    override fun onBindViewHolder(holder: VH, position: Int) = holder.onBind(getItem(position), listener)

    fun getItem(position: Int) = dataSource[position]
}