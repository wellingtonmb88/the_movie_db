package com.wellingtonmb88.themoviedb

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Build
import android.support.annotation.LayoutRes
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.util.Pair
import android.transition.Transition
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.presentation.NetworkHelper
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.jetbrains.anko.imageBitmap
import timber.log.Timber
import java.io.IOException
import java.lang.Exception
import java.lang.StringBuilder
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


fun Window.setupSharedElementEnterTransition(f: Window.() -> Transition) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        sharedElementEnterTransition = f()
    }
}

fun Activity.startActivityWithSharedElements(intent: Intent, vararg sharedElements: Pair<View, String>) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, *sharedElements)
        startActivity(intent, options.toBundle())
    } else {
        startActivity(intent)
    }
}

fun ViewGroup.runLayoutAnimation(layoutAnimationId: Int) {

    val controller = AnimationUtils.loadLayoutAnimation(context, layoutAnimationId)

    this.layoutAnimation = controller
    this.scheduleLayoutAnimation()
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View =
        LayoutInflater.from(context).inflate(layoutRes, this, false)

fun View.setupTransitionName(name: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        transitionName = name
    }
}

fun View.showSnackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}

fun ImageView.loadImage(url: String, onResourceReady: () -> Unit = {}, onLoadFailed: () -> Unit = {}) {
    if (url.isEmpty()) {
        return
    }

    val imageView = this
    imageView.imageBitmap = null
    imageView.background = null

    Glide.with(context)
            .load(url)
            .crossFade()
            .diskCacheStrategy(DiskCacheStrategy.RESULT)
            .placeholder(R.drawable.default_poster)
            .into(object : GlideDrawableImageViewTarget(imageView) {
                override fun onLoadFailed(e: Exception?, errorDrawable: Drawable?) {
                    super.onLoadFailed(e, errorDrawable)
                    onLoadFailed()
                }


                override fun onResourceReady(resource: GlideDrawable, animation: GlideAnimation<in GlideDrawable>?) {
                    super.onResourceReady(resource, animation)
                    onResourceReady()
                    imageView.imageBitmap = null
                    imageView.background = resource
                }
            })
}

fun Context.isLandScapeOrientation(): Boolean {
    val screenOrientation = (getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.rotation
    return screenOrientation == Surface.ROTATION_90 || screenOrientation == Surface.ROTATION_270
}

fun Context.checkInternetAvailable(): Boolean {
    val intent = Intent(NetworkHelper.CHECK_INTERNET_ACTION)
    val connected = isWifiConnected() || isMobileDataConnected()
    intent.putExtra(NetworkHelper.NETWORK_STATE_EXTRA, connected)
    LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    return connected
}

fun Context.isWifiConnected(): Boolean {

    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    if (cm.activeNetworkInfo != null
            && cm.activeNetworkInfo.type == ConnectivityManager.TYPE_WIFI
            && cm.activeNetworkInfo.isConnected) {
        return true
    }

    return false
}

fun Context.isMobileDataConnected(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    if (cm.activeNetworkInfo != null
            && cm.activeNetworkInfo.type == ConnectivityManager.TYPE_MOBILE
            && cm.activeNetworkInfo.isConnected) {
        return true
    }

    return false
}

fun setUpRxSchedulersForTests() {
    val immediate = object : Scheduler() {
        override fun scheduleDirect(run: Runnable, delay: Long, unit: TimeUnit): Disposable =
                super.scheduleDirect(run, 0, unit)

        override fun createWorker(): Scheduler.Worker =
                ExecutorScheduler.ExecutorWorker(Executor { it.run() })
    }

    RxJavaPlugins.setInitIoSchedulerHandler { immediate }
    RxJavaPlugins.setInitComputationSchedulerHandler { immediate }
    RxJavaPlugins.setInitNewThreadSchedulerHandler { immediate }
    RxJavaPlugins.setInitSingleSchedulerHandler { immediate }
    RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }
}

fun Any.getJsonFile(jsonFileName: String): String {
    val json: String
    val inputStream = this.javaClass.classLoader.getResourceAsStream(jsonFileName)
    try {
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        json = String(buffer)
    } catch (ex: IOException) {
        ex.printStackTrace()
        return ""
    }

    return json
}

fun getMovieIds(moviesList: List<MovieModel>): List<Int> {
    val repoIds = mutableListOf<Int>()
    moviesList.mapTo(repoIds) { it.id }
    return repoIds
}

@SuppressLint("SimpleDateFormat")
fun dateFormatter(strDate: String): String {
    try {
        val sdfSource = SimpleDateFormat("yyyy-MM-dd")

        val date = sdfSource.parse(strDate)

        val sdfDestination = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

        return sdfDestination.format(date)
    } catch (pe: ParseException) {
        Timber.e("Date Parse Exception : " + pe)
    }
    return "-"
}

fun formatMovieGenresText(genres: List<String>): String {
    val sb = StringBuilder()
    genres.forEachIndexed { index, genre ->
        if (index == 0) {
            sb.append(genre)
        } else {
            sb.append(", ").append(genre)
        }
    }
    return sb.toString()
}

fun formatMovieAdultText(isAdult: Boolean): String =
        if (isAdult) {
            "Yes"
        } else {
            "No"
        }