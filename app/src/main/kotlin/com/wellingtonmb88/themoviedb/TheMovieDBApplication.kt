package com.wellingtonmb88.themoviedb

import android.app.Activity
import android.app.Application
import com.wellingtonmb88.themoviedb.data.db.DatabaseCreator
import com.wellingtonmb88.themoviedb.di.AppComponent
import com.wellingtonmb88.themoviedb.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject


open class TheMovieDBApplication : Application(), HasActivityInjector {
    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        createComponent().inject(this)

        DatabaseCreator.createDb(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingAndroidInjector

    open fun createComponent(): AppComponent =
            DaggerAppComponent.builder()
                    .application(this)
                    .build()
}