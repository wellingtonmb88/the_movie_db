package com.wellingtonmb88.themoviedb.domain.nowplayingmovies

import com.wellingtonmb88.themoviedb.data.model.MovieModel


sealed class NowPlayingMoviesViewState {

    class Loading : NowPlayingMoviesViewState()

    class LoadingNextPage : NowPlayingMoviesViewState()

    class EmptyResult : NowPlayingMoviesViewState() {
        override fun toString() = "EmptyResult"
    }

    class EmptyResultNextPage : NowPlayingMoviesViewState() {
        override fun toString() = "EmptyResultNextPage"
    }

    class Result(val result: List<MovieModel>) : NowPlayingMoviesViewState() {
        override fun toString() = "Result{ $result }"
    }

    class ResultNextPage(val result: List<MovieModel>) : NowPlayingMoviesViewState() {
        override fun toString() = "ResultNextPage{ $result }"
    }

    class Error(val error: Throwable) : NowPlayingMoviesViewState() {
        override fun toString() = "Error{ $error }"
    }

    class ErrorNexPage(val error: Throwable) : NowPlayingMoviesViewState() {
        override fun toString() = "ErrorNexPage{ $error }"
    }
}