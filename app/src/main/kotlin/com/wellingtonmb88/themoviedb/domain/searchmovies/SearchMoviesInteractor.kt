package com.wellingtonmb88.themoviedb.domain.searchmovies

import io.reactivex.Flowable
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SearchMoviesInteractor(private val searchMoviesLoader: SearchMoviesLoader) {

    fun searchMovies(query: String): Flowable<SearchMoviesViewState> {
        Timber.d("Calling searchMovies method with param: $query")

        if (query.isEmpty()) {
            return Flowable.just(SearchMoviesViewState.NotStarted())
        }

        return searchMoviesLoader.loadFirstPage(query)
                .delay(300, TimeUnit.MILLISECONDS)
                .map { movies ->
                    if (movies.isEmpty()) {
                        Timber.d("Loading First Page. Returning empty movies.")
                        SearchMoviesViewState.EmptyResult()
                    } else {
                        Timber.d("Loading First Page. Returning movies: $movies .")
                        SearchMoviesViewState.Result(movies)
                    }
                }
                .startWith(SearchMoviesViewState.Loading())
                .onErrorReturn { error ->
                    Timber.d("Loading First Page. Returning error: $error .")
                    SearchMoviesViewState.Error(error)
                }
    }

    fun loadNextPage(query: String): Flowable<SearchMoviesViewState> {
        Timber.d("Calling loadNextPage method with param: $query")

        if (query.isEmpty()) {
            return Flowable.just(SearchMoviesViewState.NotStarted())
        }

        return searchMoviesLoader.loadNextPage(query)
                .delay(300, TimeUnit.MILLISECONDS)
                .map { movies ->
                    if (movies.isEmpty()) {
                        Timber.d("Loading Next Page. Returning empty movies.")
                        SearchMoviesViewState.EmptyResultNextPage()
                    } else {
                        Timber.d("Loading Next Page. Returning movies: $movies .")
                        SearchMoviesViewState.ResultNextPage(movies)
                    }
                }
                .startWith(SearchMoviesViewState.LoadingNextPage())
                .onErrorReturn { error ->
                    Timber.d("Loading Next Page. Returning error: $error .")
                    SearchMoviesViewState.ErrorNexPage(error)
                }
    }
}