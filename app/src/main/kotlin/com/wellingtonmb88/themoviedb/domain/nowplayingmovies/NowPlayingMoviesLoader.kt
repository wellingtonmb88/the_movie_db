package com.wellingtonmb88.themoviedb.domain.nowplayingmovies

import com.wellingtonmb88.themoviedb.data.TheMovieDBRepository
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import io.reactivex.Flowable

class NowPlayingMoviesLoader(private val theMovieDBRepository: TheMovieDBRepository) {

    private var currentPage = 2
    private var endReached = false

    fun loadFirstPage(): Flowable<List<MovieModel>> {
        endReached = false
        currentPage = 2
        return theMovieDBRepository.getNowPlayingMoviesList(1)
    }

    fun loadNextPage(): Flowable<List<MovieModel>> {
        if(endReached) {
            return Flowable.empty()
        }
        return theMovieDBRepository.getNowPlayingMoviesList(currentPage).doOnNext { result ->
            currentPage++
            if(result.isEmpty()) {
                endReached = true
            }
        }
    }
}