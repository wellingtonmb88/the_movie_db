package com.wellingtonmb88.themoviedb.domain.searchmovies

import com.wellingtonmb88.themoviedb.data.model.MovieModel

sealed class SearchMoviesViewState {

    class NotStarted : SearchMoviesViewState()

    class Loading : SearchMoviesViewState()

    class LoadingNextPage : SearchMoviesViewState()

    class EmptyResult : SearchMoviesViewState() {
        override fun toString() = "EmptyResult"
    }

    class EmptyResultNextPage : SearchMoviesViewState() {
        override fun toString() = "EmptyResultNextPage"
    }

    class Result(val result: List<MovieModel>) : SearchMoviesViewState() {
        override fun toString() = "Result{ $result }"
    }

    class ResultNextPage(val result: List<MovieModel>) : SearchMoviesViewState() {
        override fun toString() = "ResultNextPage{ $result }"
    }

    class Error(val error: Throwable) : SearchMoviesViewState() {
        override fun toString() = "Error{ $error }"
    }

    class ErrorNexPage(val error: Throwable) : SearchMoviesViewState() {
        override fun toString() = "ErrorNexPage{ $error }"
    }
}