package com.wellingtonmb88.themoviedb.domain.searchmovies

import com.wellingtonmb88.themoviedb.data.TheMovieDBRepository
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import io.reactivex.Flowable

class SearchMoviesLoader(private val theMovieDBRepository: TheMovieDBRepository) {

    private var currentPage = 2
    private var endReached = false

    fun loadFirstPage(query: String): Flowable<List<MovieModel>> {
        endReached = false
        currentPage = 2
        return theMovieDBRepository.searchMoviesList(query, 1)
    }

    fun loadNextPage(query: String): Flowable<List<MovieModel>> {
        if (endReached) {
            return Flowable.empty()
        }
        return theMovieDBRepository.searchMoviesList(query, currentPage).doOnNext { result ->
            currentPage++
            if (result.isEmpty()) {
                endReached = true
            }
        }
    }
}