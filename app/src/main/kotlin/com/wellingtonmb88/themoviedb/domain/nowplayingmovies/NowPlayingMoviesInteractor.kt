package com.wellingtonmb88.themoviedb.domain.nowplayingmovies

import io.reactivex.Flowable
import timber.log.Timber
import java.util.concurrent.TimeUnit

class NowPlayingMoviesInteractor(private val nowPlayingMoviesLoader: NowPlayingMoviesLoader) {

    fun loadListByAverageHigherThan(average: Double): Flowable<NowPlayingMoviesViewState> {
        Timber.d("Calling searchMovies method with param: $average")
        return nowPlayingMoviesLoader.loadFirstPage()
                .delay(300, TimeUnit.MILLISECONDS)
                .map { movies -> movies.filter { movie -> movie.voteAverage >= average } }
                .map { movies ->
                    if (movies.isEmpty()) {
                        Timber.d("Loading First Page. Returning empty movies.")
                        NowPlayingMoviesViewState.EmptyResult()
                    } else {
                        Timber.d("Loading First Page. Returning movies: $movies .")
                        NowPlayingMoviesViewState.Result(movies)
                    }
                }
                .startWith(NowPlayingMoviesViewState.Loading())
                .onErrorReturn { error ->
                    Timber.d("Loading First Page. Returning error: $error .")
                    NowPlayingMoviesViewState.Error(error)
                }
    }

    fun loadNextPage(average: Double): Flowable<NowPlayingMoviesViewState> {
        Timber.d("Calling loadNextPage method with param: $average")
        return nowPlayingMoviesLoader.loadNextPage()
                .delay(300, TimeUnit.MILLISECONDS)
                .map { movies -> movies.filter { movie -> movie.voteAverage >= average } }
                .map { movies ->
                    if (movies.isEmpty()) {
                        Timber.d("Loading Next Page. Returning empty movies.")
                        NowPlayingMoviesViewState.EmptyResultNextPage()
                    } else {
                        Timber.d("Loading Next Page. Returning movies: $movies .")
                        NowPlayingMoviesViewState.ResultNextPage(movies)
                    }
                }
                .startWith(NowPlayingMoviesViewState.LoadingNextPage())
                .onErrorReturn { error ->
                    Timber.d("Loading Next Page. Returning error: $error .")
                    NowPlayingMoviesViewState.ErrorNexPage(error)
                }
    }
}