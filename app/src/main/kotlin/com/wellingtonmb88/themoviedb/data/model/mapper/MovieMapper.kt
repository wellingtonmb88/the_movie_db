package com.wellingtonmb88.themoviedb.data.model.mapper

import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.remote.model.MovieModelApi

class MovieMapper : ModelMapper<MovieModelApi, MovieModel>() {
    override fun transform(modelApi: MovieModelApi?): MovieModel? {
        if (modelApi != null) {

            return MovieModel(
                    modelApi.id ?: 0,
                    modelApi.title ?: "",
                    modelApi.originalTitle ?: "",
                    modelApi.overview ?: "",
                    modelApi.releaseDate ?: "",
                    "http://image.tmdb.org/t/p/w185" + modelApi.posterPath,
                    "http://image.tmdb.org/t/p/w185" + modelApi.backdropPath,
                    modelApi.adult ?: false,
                    modelApi.voteAverage ?: 0.0,
                    modelApi.genres
            )
        }
        return null
    }
}