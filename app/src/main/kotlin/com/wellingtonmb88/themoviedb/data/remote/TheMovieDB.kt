package com.wellingtonmb88.themoviedb.data.remote

import com.wellingtonmb88.themoviedb.data.remote.model.GenresModelApi
import com.wellingtonmb88.themoviedb.data.remote.model.MoviesModelApi
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface TheMovieDB {

    @GET("movie/now_playing")
    fun getNowPlayingMoviesList(@Query("page") page: String,
                                @Query("language") language: String): Single<MoviesModelApi>

    @GET("search/movie?include_adult=true")
    fun searchMoviesList(@Query("query") query: String, @Query("page") page: String,
                         @Query("language") language: String): Single<MoviesModelApi>

    @GET("genre/movie/list")
    fun getGenresList(@Query("language") language: String): Single<GenresModelApi>
}