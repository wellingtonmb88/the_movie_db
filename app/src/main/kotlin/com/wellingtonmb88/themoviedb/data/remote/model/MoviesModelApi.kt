package com.wellingtonmb88.themoviedb.data.remote.model

import com.google.gson.annotations.SerializedName

class MoviesModelApi {

    @SerializedName("results")
    val moviesList: List<MovieModelApi> = listOf()

    fun getMovieIds(): List<Int> {
        val repoIds = mutableListOf<Int>()
        moviesList.mapTo(repoIds) { it.id ?: -1 }
        return repoIds
    }
}