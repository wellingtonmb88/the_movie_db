package com.wellingtonmb88.themoviedb.data.model.mapper

abstract class ModelMapper<in ModelApi, out Model> {

    abstract fun transform(modelApi: ModelApi?): Model?

    fun transform(collection: Collection<ModelApi>?): List<Model> {
        val list = mutableListOf<Model>()
        if (collection != null) {
            var model: Model?
            for (entity in collection) {
                model = transform(entity)
                if (model != null) {
                    list.add(model)
                }
            }
        }
        return list
    }
}