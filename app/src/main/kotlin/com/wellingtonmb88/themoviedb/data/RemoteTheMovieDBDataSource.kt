package com.wellingtonmb88.themoviedb.data

import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.model.mapper.MovieMapper
import com.wellingtonmb88.themoviedb.data.remote.TheMovieDBApi
import com.wellingtonmb88.themoviedb.getMovieIds
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RemoteTheMovieDBDataSource
@Inject
constructor(private val theMovieDBApi: TheMovieDBApi,
            private val localTheMovieDBDataSource: LocalTheMovieDBDataSource) : TheMovieDBDataSource {

    private val LANG = "en-US"

    override fun getNowPlayingMoviesList(page: Int): Flowable<List<MovieModel>> {

        return theMovieDBApi.createService().getGenresList(LANG)
                .subscribeOn(Schedulers.io())
                .map { genresModelApi ->
                    genresModelApi.genresList
                }
                .toFlowable()
                .concatMap { genresList ->
                    val blockingGet = theMovieDBApi.createService()
                            .getNowPlayingMoviesList(page.toString(), LANG).blockingGet()

                    blockingGet.moviesList.map { movieApi ->
                        val genreNameList: MutableList<String> = mutableListOf()
                        genresList.forEach { genre ->
                            if (movieApi.genreIds?.contains(genre.id.toInt()) == true) {
                                genreNameList.add(genre.name)
                            }
                        }
                        movieApi.genres.addAll(genreNameList)
                    }

                    Flowable.just(blockingGet.moviesList)
                }
                .map { movieModelApi ->
                    val list = MovieMapper().transform(movieModelApi)
                    localTheMovieDBDataSource.saveNowPlayingMoviesList(page, getMovieIds(list), list)
                    list
                }
    }

    override fun searchMoviesList(query: String, page: Int): Flowable<List<MovieModel>> {
        return theMovieDBApi.createService().getGenresList(LANG)
                .subscribeOn(Schedulers.io())
                .map { genresModelApi ->
                    genresModelApi.genresList
                }
                .toFlowable()
                .concatMap { genresList ->
                    val blockingGet = theMovieDBApi.createService()
                            .searchMoviesList(query, page.toString(), LANG).blockingGet()

                    blockingGet.moviesList.map { movieApi ->
                        val genreNameList: MutableList<String> = mutableListOf()
                        genresList.forEach { genre ->
                            if (movieApi.genreIds?.contains(genre.id.toInt()) == true) {
                                genreNameList.add(genre.name)
                            }
                        }
                        movieApi.genres.addAll(genreNameList)
                    }

                    Flowable.just(blockingGet.moviesList)
                }
                .map { movieModelApi ->
                    MovieMapper().transform(movieModelApi)
                }
    }
}