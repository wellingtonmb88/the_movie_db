package com.wellingtonmb88.themoviedb.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.model.NowPlayingMoviesResultModel


@Database(entities = arrayOf(NowPlayingMoviesResultModel::class, MovieModel::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun theMovieDBDao(): TheMovieDBDao

    companion object {
        const val DATABASE_NAME = "the-movie-db"
    }
}