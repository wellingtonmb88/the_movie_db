package com.wellingtonmb88.themoviedb.data.remote.model

import com.google.gson.annotations.SerializedName

class GenresModelApi {
    @SerializedName("genres")
    val genresList: List<GenreModelApi> = listOf()
}