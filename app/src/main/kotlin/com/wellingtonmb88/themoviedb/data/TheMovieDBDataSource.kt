package com.wellingtonmb88.themoviedb.data

import com.wellingtonmb88.themoviedb.data.model.MovieModel
import io.reactivex.Flowable


interface TheMovieDBDataSource {

    fun getNowPlayingMoviesList(page: Int): Flowable<List<MovieModel>>

    fun searchMoviesList(query: String, page: Int): Flowable<List<MovieModel>> = Flowable.empty()

    fun saveNowPlayingMoviesList(page: Int, movieIds: List<Int>, moviesList: List<MovieModel>) = Unit
}