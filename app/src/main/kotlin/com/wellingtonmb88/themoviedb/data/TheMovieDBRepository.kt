package com.wellingtonmb88.themoviedb.data

import com.wellingtonmb88.themoviedb.data.model.MovieModel
import io.reactivex.Flowable

class TheMovieDBRepository(private val remoteTheMovieDBDataSource: RemoteTheMovieDBDataSource,
                           private val localTheMovieDBDataSource: LocalTheMovieDBDataSource) : TheMovieDBDataSource {

    override fun getNowPlayingMoviesList(page: Int): Flowable<List<MovieModel>> {
        return localTheMovieDBDataSource.getNowPlayingMoviesList(page)
                .firstOrError()
                .onErrorResumeNext {
                    remoteTheMovieDBDataSource.getNowPlayingMoviesList(page).singleOrError()
                }.toFlowable()
    }

    override fun searchMoviesList(query: String, page: Int): Flowable<List<MovieModel>> =
            remoteTheMovieDBDataSource.searchMoviesList(query, page)
}