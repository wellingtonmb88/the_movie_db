package com.wellingtonmb88.themoviedb.data.remote.model

import com.google.gson.annotations.SerializedName

class MovieModelApi {

    var id: Int? = 0

    var title: String? = ""

    @SerializedName("original_title")
    var originalTitle: String? = ""

    var overview: String? = ""

    @SerializedName("release_date")
    var releaseDate: String? = ""

    @SerializedName("poster_path")
    var posterPath: String? = ""

    @SerializedName("backdrop_path")
    var backdropPath: String? = ""

    var adult: Boolean? = false

    @SerializedName("vote_average")
    var voteAverage: Double? = 0.0

    @SerializedName("genre_ids")
    var genreIds: List<Int>? = listOf()

    val genres: MutableList<String> = mutableListOf()
}