package com.wellingtonmb88.themoviedb.data.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters

@Entity(tableName = "nowplayingmoviesresults")
@TypeConverters(IntegerTypeConverters::class)
data class NowPlayingMoviesResultModel(@PrimaryKey
                                       @ColumnInfo(name = "page_number")
                                       val pageNumber: Int,
                                       @ColumnInfo(name = "movie_ids")
                                       val movieIds: List<Int>)