package com.wellingtonmb88.themoviedb.data.resolution

abstract class ResolutionByCase : Resolution {

    override fun onHttpException(code: Int) =
            when (code) {
                503 -> onServiceUnavailable()
                404 -> onNotFound()
                else -> onInternalServerError()
            }

    abstract fun onInternalServerError(): ResolutionViewState

    abstract fun onNotFound(): ResolutionViewState

    abstract fun onServiceUnavailable(): ResolutionViewState

    abstract fun onInternetUnavailable(): ResolutionViewState


    sealed class ResolutionViewState {
        class NotFound : ResolutionViewState()

        class ServiceUnavailable : ResolutionViewState()

        class InternalServerError : ResolutionViewState()

        class InternetUnavailable : ResolutionViewState()
    }
}