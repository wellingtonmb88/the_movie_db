package com.wellingtonmb88.themoviedb.data.remote

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TheMovieDBApi
@Inject
constructor(private val retrofit: Retrofit) {
    fun createService(): TheMovieDB = retrofit.create(TheMovieDB::class.java)
}