package com.wellingtonmb88.themoviedb.data

import com.wellingtonmb88.themoviedb.data.db.TheMovieDBDao
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.model.NowPlayingMoviesResultModel
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalTheMovieDBDataSource
@Inject
constructor(private val theMovieDBDao: TheMovieDBDao) : TheMovieDBDataSource {

    override fun getNowPlayingMoviesList(page: Int): Flowable<List<MovieModel>> {
        return theMovieDBDao.loadAllNowPlayingMoviesResults(page)
                .subscribeOn(Schedulers.io())
                .map {
                    theMovieDBDao.loadAllMovieModelsByIds(it.movieIds).blockingFirst()
                }.toFlowable()
    }

    override fun saveNowPlayingMoviesList(page: Int, movieIds: List<Int>, moviesList: List<MovieModel>) {
        theMovieDBDao.insertNowPlayingMoviesResultModel(NowPlayingMoviesResultModel(page, movieIds))
        theMovieDBDao.insertMovieModels(moviesList)
    }
}