package com.wellingtonmb88.themoviedb.data.model

import android.arch.persistence.room.TypeConverter
import android.arch.persistence.room.util.StringUtil
import java.util.*

class IntegerTypeConverters {
    @TypeConverter
    fun stringToIntList(data: String?): List<Int> {
        if (data == null) {
            return Collections.emptyList()
        }
        return StringUtil.splitToIntList(data)!!.toList()
    }

    @TypeConverter
    fun intListToString(ints: List<Int>): String = StringUtil.joinIntoString(ints)!!
}