package com.wellingtonmb88.themoviedb.data.resolution


interface RxHttpResolution {
    fun onHttpException(code: Int): ResolutionByCase.ResolutionViewState
}

interface Resolution : RxHttpResolution