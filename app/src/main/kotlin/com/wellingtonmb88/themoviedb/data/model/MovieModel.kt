package com.wellingtonmb88.themoviedb.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "movies")
@TypeConverters(StringTypeConverters::class)
data class MovieModel(@PrimaryKey val id: Int,
                      val title: String,
                      val originalTitle: String,
                      val overview: String,
                      val releaseDate: String,
                      val posterPath: String,
                      val backdropPath: String,
                      val adult: Boolean,
                      val voteAverage: Double,
                      val genres: List<String>) : Parcelable