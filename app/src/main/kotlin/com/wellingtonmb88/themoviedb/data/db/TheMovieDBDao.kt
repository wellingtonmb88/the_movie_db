package com.wellingtonmb88.themoviedb.data.db

import android.arch.persistence.room.*
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.model.NowPlayingMoviesResultModel
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface TheMovieDBDao {

    @Query("SELECT * FROM nowplayingmoviesresults WHERE page_number = :pageNumber")
    fun loadAllNowPlayingMoviesResults(pageNumber: Int): Single<NowPlayingMoviesResultModel>

    @Query("SELECT * FROM movies WHERE id in (:movieIds)")
    fun loadAllMovieModelsByIds(movieIds: List<Int>): Flowable<List<MovieModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieModels(moviesList: List<MovieModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNowPlayingMoviesResultModel(nowPlayingMoviesResultModel: NowPlayingMoviesResultModel)

    @Query("DELETE FROM nowplayingmoviesresults")
    fun deleteAllNowPlayingMoviesResults()

    @Query("DELETE FROM movies")
    fun deleteAllMovieModels()
}