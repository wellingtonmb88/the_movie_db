package com.wellingtonmb88.themoviedb.data.model

import android.arch.persistence.room.TypeConverter
import java.util.*


class StringTypeConverters {

    @TypeConverter
    fun stringToStringList(input: String?): List<String> {
        if (input == null) {
            return Collections.emptyList()
        }

        val result = ArrayList<String>()
        val tokenizer = StringTokenizer(input, ",")
        while (tokenizer.hasMoreElements()) {
            val item = tokenizer.nextToken()
            result.add(item)
        }
        return result
    }

    @TypeConverter
    fun stringListToString(input: List<String>): String {

        val size = input.size
        if (size == 0) {
            return ""
        }
        val sb = StringBuilder()
        for (i in 0 until size) {
            sb.append(input[i])
            if (i < size - 1) {
                sb.append(",")
            }
        }
        return sb.toString()
    }
}