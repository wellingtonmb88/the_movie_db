package com.wellingtonmb88.themoviedb.di

import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesActivity
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = arrayOf(NowPlayingMoviesModule::class))
    abstract fun bindNowPlayingMoviesActivity(): NowPlayingMoviesActivity

    @ContributesAndroidInjector(modules = arrayOf(SearchMoviesModule::class))
    abstract fun bindSearchMoviesActivity(): SearchMoviesActivity
}