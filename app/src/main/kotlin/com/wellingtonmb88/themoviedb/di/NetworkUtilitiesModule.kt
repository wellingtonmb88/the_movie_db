package com.wellingtonmb88.themoviedb.di

import com.wellingtonmb88.themoviedb.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import okhttp3.Interceptor
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkUtilitiesModule {

    @Provides
    @Singleton
    @Named("HTTP_URL")
    fun providesHttpUrl() = BuildConfig.THE_MOVIE_DB_URL

    @Provides
    @IntoSet
    fun providesWebServerInterceptor(): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()

            val url = original.url().newBuilder()
                    .addQueryParameter("api_key", BuildConfig.THE_MOVIE_DB_API_KEY)
                    .build()

            val requestBuilder = original.newBuilder().url(url)

            return@Interceptor chain.proceed(requestBuilder.build())
        }
    }
}