package com.wellingtonmb88.themoviedb.di

import android.app.Application
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.wellingtonmb88.themoviedb.data.LocalTheMovieDBDataSource
import com.wellingtonmb88.themoviedb.data.RemoteTheMovieDBDataSource
import com.wellingtonmb88.themoviedb.data.TheMovieDBRepository
import com.wellingtonmb88.themoviedb.data.db.DatabaseCreator
import com.wellingtonmb88.themoviedb.data.db.TheMovieDBDao
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Named
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    @Singleton
    internal fun providesTheMovieDBRepository(remoteDataSource: RemoteTheMovieDBDataSource,
                                              localDataSource: LocalTheMovieDBDataSource): TheMovieDBRepository =
            TheMovieDBRepository(remoteDataSource, localDataSource)

    @Provides
    @Singleton
    internal fun providesTheMovieDBDao(): TheMovieDBDao = DatabaseCreator.database.theMovieDBDao()

    @Provides
    @Singleton
    internal fun providesGson(): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create()
    }

    @Provides
    @Singleton
    internal fun providesHttpCache(application: Application): Cache {
        val cacheSize: Long = 10 * 1024 * 1024
        return Cache(application.cacheDir, cacheSize)
    }

    @Provides
    @Singleton
    internal fun providesOkHttpClientBuilder(): OkHttpClient.Builder = OkHttpClient.Builder()

    @Provides
    @Singleton
    internal fun providesOkhttpClient(cache: Cache, builder: OkHttpClient.Builder,
                                      interceptors: Set<@JvmSuppressWildcards Interceptor>): OkHttpClient {
        interceptors.forEach { builder.addInterceptor(it) }

        val httpLoggingInterceptor = HttpLoggingInterceptor({ message -> Timber.tag("OkHttp").d(message) })
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return builder
                .addInterceptor(httpLoggingInterceptor)
                .cache(cache)
                .build()
    }

    @Provides
    @Singleton
    internal fun providesRetrofit(gson: Gson,
                                  okHttpClient: OkHttpClient,
                                  @Named("HTTP_URL") url: String): Retrofit {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(url)
                .client(okHttpClient)
                .build()
    }
}