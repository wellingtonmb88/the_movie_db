package com.wellingtonmb88.themoviedb.di

import com.wellingtonmb88.themoviedb.data.TheMovieDBRepository
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesLoader
import com.wellingtonmb88.themoviedb.presentation.resolution.UIResolution
import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesViewModel
import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class NowPlayingMoviesModule {

    @Provides
    internal fun providesNowPlayingMoviesLoader(theMovieDBRepository: TheMovieDBRepository): NowPlayingMoviesLoader =
            NowPlayingMoviesLoader(theMovieDBRepository)

    @Provides
    internal fun providesNowPlayingMoviesInteractor(nowPlayingMoviesLoader: NowPlayingMoviesLoader): NowPlayingMoviesInteractor =
            NowPlayingMoviesInteractor(nowPlayingMoviesLoader)

    @Provides
    internal fun providesNowPlayingMoviesViewModel(interactor: NowPlayingMoviesInteractor, resolution: UIResolution): NowPlayingMoviesViewModel =
            NowPlayingMoviesViewModel(interactor, resolution)

    @Provides
    internal fun provideNowPlayingMoviesViewModelFactory(viewModel: NowPlayingMoviesViewModel): NowPlayingMoviesViewModelFactory =
            NowPlayingMoviesViewModelFactory(viewModel)
}