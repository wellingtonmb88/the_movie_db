package com.wellingtonmb88.themoviedb.di

import com.wellingtonmb88.themoviedb.data.TheMovieDBRepository
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesLoader
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesLoader
import com.wellingtonmb88.themoviedb.presentation.resolution.UIResolution
import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesViewModel
import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesViewModelFactory
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesViewModel
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class SearchMoviesModule {

    @Provides
    internal fun providesSearchMoviesLoader(theMovieDBRepository: TheMovieDBRepository): SearchMoviesLoader =
            SearchMoviesLoader(theMovieDBRepository)

    @Provides
    internal fun providesSearchMoviesInteractor(nowPlayingMoviesLoader: SearchMoviesLoader): SearchMoviesInteractor =
            SearchMoviesInteractor(nowPlayingMoviesLoader)

    @Provides
    internal fun providesSearchMoviesViewModel(interactor: SearchMoviesInteractor, resolution: UIResolution): SearchMoviesViewModel =
            SearchMoviesViewModel(interactor, resolution)

    @Provides
    internal fun provideSearchMoviesViewModelFactory(viewModel: SearchMoviesViewModel): SearchMoviesViewModelFactory =
            SearchMoviesViewModelFactory(viewModel)
}