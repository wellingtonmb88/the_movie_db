package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesLoader
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesViewState
import io.reactivex.Flowable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.HttpException
import retrofit2.Response
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class NowPlayingMoviesInteractorTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val AVERAGE = 0.5

    private var nowPlayingMoviesLoaderMock = mock<NowPlayingMoviesLoader>()

    private lateinit var interactor: NowPlayingMoviesInteractor

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulersForTests()
        interactor = NowPlayingMoviesInteractor(nowPlayingMoviesLoaderMock)
    }

    //Load Now Playing Movies Action

    @Test
    fun loadNowPlayingMoviesAction_ensureItGetsEmptyResultSuccessfully() {

        val mockListModel: List<MovieModel> = listOf()

        whenever(nowPlayingMoviesLoaderMock.loadFirstPage()).thenReturn(Flowable.just(mockListModel))

        interactor.loadListByAverageHigherThan(AVERAGE)

        verify(nowPlayingMoviesLoaderMock).loadFirstPage()

        val testSubscriber = interactor.loadListByAverageHigherThan(AVERAGE).test()

        assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is NowPlayingMoviesViewState.Loading -> {
                        assertTrue(true)
                    }
                    is NowPlayingMoviesViewState.EmptyResult -> {

                        assertEquals(viewState.toString(), "EmptyResult")
                    }
                    else -> {
                        fail()
                    }
                }
            }
        }
    }

    @Test
    fun loadNowPlayingMoviesAction_ensureItGetsResultSuccessfully() {

        val mockListModel = getMockListModel()

        whenever(nowPlayingMoviesLoaderMock.loadFirstPage()).thenReturn(Flowable.just(mockListModel))

        interactor.loadListByAverageHigherThan(AVERAGE)

        verify(nowPlayingMoviesLoaderMock).loadFirstPage()

        val testSubscriber = interactor.loadListByAverageHigherThan(AVERAGE).test()

        assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is NowPlayingMoviesViewState.Loading -> {
                        assertTrue(true)
                    }
                    is NowPlayingMoviesViewState.Result -> {
                        assertEquals(mockListModel, viewState.result)

                        assertEquals(viewState.toString(), "Result{ $mockListModel }")
                    }
                    else -> {
                        fail()
                    }
                }
            }
        }

    }

    @Test
    fun loadNowPlayingMoviesAction_ensureItShowsServiceUnavailableResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_UNAVAILABLE
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val httpException = HttpException(responseError)

        whenever(nowPlayingMoviesLoaderMock.loadFirstPage()).thenReturn(Flowable.error(httpException))

        interactor.loadListByAverageHigherThan(AVERAGE)

        verify(nowPlayingMoviesLoaderMock).loadFirstPage()

        val testSubscriber = interactor.loadListByAverageHigherThan(AVERAGE).test()

        assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is NowPlayingMoviesViewState.Loading -> {
                        assertTrue(true)
                    }
                    is NowPlayingMoviesViewState.Error -> {
                        assertEquals(httpException, viewState.error)
                        assertEquals(viewState.toString(), "Error{ $httpException }")
                    }
                    else -> {
                        fail()
                    }
                }
            }
        }
    }

    //Load Next Page

    @Test
    fun loadNexPageAction_ensureItGetsEmptyResultSuccessfully() {

        val mockListModel: List<MovieModel> = listOf()

        whenever(nowPlayingMoviesLoaderMock.loadNextPage()).thenReturn(Flowable.just(mockListModel))

        interactor.loadNextPage(AVERAGE)

        verify(nowPlayingMoviesLoaderMock).loadNextPage()

        val testSubscriber = interactor.loadNextPage(AVERAGE).test()

        assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is NowPlayingMoviesViewState.LoadingNextPage -> {
                        assertTrue(true)
                    }
                    is NowPlayingMoviesViewState.EmptyResultNextPage -> {

                        assertEquals(viewState.toString(), "EmptyResultNextPage")
                    }
                    else -> {
                        fail()
                    }
                }
            }
        }
    }

    @Test
    fun loadNexPageAction_ensureItGetsResultSuccessfully() {

        val mockListModel = getMockListModel()

        whenever(nowPlayingMoviesLoaderMock.loadNextPage()).thenReturn(Flowable.just(mockListModel))

        interactor.loadNextPage(AVERAGE)

        verify(nowPlayingMoviesLoaderMock).loadNextPage()

        val testSubscriber = interactor.loadNextPage(AVERAGE).test()

        assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is NowPlayingMoviesViewState.LoadingNextPage -> {
                        assertTrue(true)
                    }
                    is NowPlayingMoviesViewState.ResultNextPage -> {
                        assertEquals(mockListModel, viewState.result)
                        assertEquals(viewState.toString(), "ResultNextPage{ $mockListModel }")
                    }
                    else -> {
                        fail()
                    }
                }
            }
        }
    }

    @Test
    fun loadNexPageAction_ensureItShowsServiceUnavailableResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_UNAVAILABLE
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val httpException = HttpException(responseError)

        whenever(nowPlayingMoviesLoaderMock.loadNextPage()).thenReturn(Flowable.error(httpException))

        interactor.loadNextPage(AVERAGE)

        verify(nowPlayingMoviesLoaderMock).loadNextPage()

        val testSubscriber = interactor.loadNextPage(AVERAGE).test()

        assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is NowPlayingMoviesViewState.LoadingNextPage -> {
                        assertTrue(true)
                    }
                    is NowPlayingMoviesViewState.ErrorNexPage -> {
                        assertEquals(httpException, viewState.error)
                        assertEquals(viewState.toString(), "ErrorNexPage{ $httpException }")
                    }
                    else -> {
                        fail()
                    }
                }
            }
        }
    }

    private fun getMockListModel(): List<MovieModel> {
        val MovieModel = MovieModel(id = 1, title = "title", originalTitle = "", overview = "",
                releaseDate = "", adult = false, posterPath = "", backdropPath = "",
                voteAverage = AVERAGE, genres = listOf())
        return arrayListOf(MovieModel, MovieModel, MovieModel, MovieModel)
    }
}