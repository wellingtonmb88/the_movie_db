package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.nowplayingmovies.NowPlayingMoviesViewState
import com.wellingtonmb88.themoviedb.presentation.resolution.UIResolution
import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesViewModel
import io.reactivex.Flowable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.HttpURLConnection


@RunWith(JUnit4::class)
class NowPlayingMoviesViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val AVERAGE = NowPlayingMoviesViewModel.AVERAGE

    private var interactorMock = mock<NowPlayingMoviesInteractor>()

    private var uiResolutionMock = mock<UIResolution>()

    private lateinit var viewModel: NowPlayingMoviesViewModel

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulersForTests()
        viewModel = NowPlayingMoviesViewModel(interactorMock, uiResolutionMock)
    }

    //Load Now Playing Movies Action

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItGetsEmptyResultSuccessfully() {
        val emptyResultViewState = NowPlayingMoviesViewState.EmptyResult()

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(emptyResultViewState))

        val observer = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(observer)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(observer).onChanged(listOf())

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(emptyResultViewState)
    }

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItGetsResultSuccessfully() {
        val mockListModel = getMockListModel()

        val resultViewState = NowPlayingMoviesViewState.Result(mockListModel)

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(resultViewState))

        val observer = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(observer)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(observer).onChanged(mockListModel)

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(resultViewState)
    }

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItShowsLoadingSuccessfully() {

        val loadingViewState = NowPlayingMoviesViewState.Loading()

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(loadingViewState))

        val isLoadingObserver = mock<Observer<Boolean>>()
        viewModel.isLoadingLiveData.observeForever(isLoadingObserver)

        val observer = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(observer)

        verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(isLoadingObserver).onChanged(true)

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(loadingViewState)

    }

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItShowsErrorSuccessfully() {

        val errorViewState = NowPlayingMoviesViewState.Error(Throwable("Generic Error!"))

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(errorViewState))

        val showErrorSearchObserver = mock<Observer<Boolean>>()
        viewModel.showErrorNowPlayingMoviesLiveData.observeForever(showErrorSearchObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(showErrorSearchObserver).onChanged(true)

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(errorViewState)

    }

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItShowsInternalServerResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_INTERNAL_ERROR
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError500 = Response.error<Any>(errorCode, responseBody)

        val internalServerError = ResolutionByCase.ResolutionViewState.InternalServerError()
        val errorViewState = NowPlayingMoviesViewState.Error(HttpException(responseError500))

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onHttpException(errorCode)).thenReturn(internalServerError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(internalServerError)

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(errorViewState)

    }

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItShowsServiceUnavailableResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_UNAVAILABLE
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val serviceUnavailableError = ResolutionByCase.ResolutionViewState.ServiceUnavailable()
        val errorViewState = NowPlayingMoviesViewState.Error(HttpException(responseError))

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onHttpException(errorCode)).thenReturn(serviceUnavailableError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(serviceUnavailableError)

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(errorViewState)
    }

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItShowsNotFoundResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_NOT_FOUND
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val notFoundError = ResolutionByCase.ResolutionViewState.NotFound()
        val errorViewState = NowPlayingMoviesViewState.Error(HttpException(responseError))

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onHttpException(errorCode)).thenReturn(notFoundError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(notFoundError)

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(errorViewState)
    }

    @Test
    fun viewModelLoadNowPlayingMoviesAction_ensureItShowsInternetUnavailableResolutionErrorSuccessfully() {

        val internetUnavailableError = ResolutionByCase.ResolutionViewState.InternetUnavailable()
        val errorViewState = NowPlayingMoviesViewState.Error(IOException())

        whenever(interactorMock.loadListByAverageHigherThan(AVERAGE)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onInternetUnavailable()).thenReturn(internetUnavailableError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNowPlayingMovies()

        verify(interactorMock).loadListByAverageHigherThan(AVERAGE)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(internetUnavailableError)

        val testObserverViewState = interactorMock.loadListByAverageHigherThan(AVERAGE).test()
        testObserverViewState.assertValue(errorViewState)
    }

    //Load Next Page

    @Test
    fun viewModelLoadNextPageAction_ensureItGetsEmptyResultSuccessfully() {
        val emptyResultViewState = NowPlayingMoviesViewState.EmptyResultNextPage()

        whenever(interactorMock.loadNextPage(AVERAGE)).thenReturn(Flowable.just(emptyResultViewState))

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage()

        verify(interactorMock).loadNextPage(AVERAGE)

        verify(moviesListObserver).onChanged(listOf())

        val testObserverViewState = interactorMock.loadNextPage(AVERAGE).test()
        testObserverViewState.assertValue(emptyResultViewState)
    }

    @Test
    fun viewModelLoadNextPageAction_ensureItGetsResultSuccessfully() {

        val arrayListOfModel = getMockListModel()
        val resultViewState = NowPlayingMoviesViewState.ResultNextPage(arrayListOfModel)

        whenever(interactorMock.loadNextPage(AVERAGE)).thenReturn(Flowable.just(resultViewState))

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage()

        verify(interactorMock).loadNextPage(AVERAGE)

        verify(moviesListObserver).onChanged(arrayListOfModel)

        val testObserverViewState = interactorMock.loadNextPage(AVERAGE).test()
        testObserverViewState.assertValue(resultViewState)
    }

    @Test
    fun viewModelLoadNextPageAction_ensureItShowsLoadingSuccessfully() {

        val loadingViewState = NowPlayingMoviesViewState.LoadingNextPage()

        whenever(interactorMock.loadNextPage(AVERAGE)).thenReturn(Flowable.just(loadingViewState))

        val isLoadingObserver = mock<Observer<Boolean>>()
        viewModel.isLoadingNextPageLiveData.observeForever(isLoadingObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage()

        verify(interactorMock).loadNextPage(AVERAGE)

        verify(isLoadingObserver).onChanged(true)

        val testObserverViewState = interactorMock.loadNextPage(AVERAGE).test()
        testObserverViewState.assertValue(loadingViewState)
    }

    @Test
    fun viewModelLoadNextPageAction_ensureItShowsErrorSuccessfully() {

        val loadingViewState = NowPlayingMoviesViewState.ErrorNexPage(Throwable("Generic Error!"))

        whenever(interactorMock.loadNextPage(AVERAGE)).thenReturn(Flowable.just(loadingViewState))

        val showErrorSearchObserver = mock<Observer<Boolean>>()
        viewModel.showErrorLoadingNexPageLiveData.observeForever(showErrorSearchObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage()

        verify(interactorMock).loadNextPage(AVERAGE)

        verify(showErrorSearchObserver).onChanged(true)

        val testObserverViewState = interactorMock.loadNextPage(AVERAGE).test()
        testObserverViewState.assertValue(loadingViewState)
    }

    private fun getMockListModel(): List<MovieModel> {
        val MovieModel = MovieModel(id = 1, title = "title", originalTitle = "", overview = "",
                releaseDate = "", adult = false, posterPath = "", backdropPath = "",
                voteAverage = AVERAGE, genres = listOf())
        return arrayListOf(MovieModel, MovieModel, MovieModel, MovieModel)
    }
}