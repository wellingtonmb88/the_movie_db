package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.presentation.resolution.UIResolution
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.net.HttpURLConnection


@RunWith(JUnit4::class)
class UIResoluitonTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private lateinit var uiResolution: UIResolution

    @Before
    @Throws(Exception::class)
    fun setUp() {
        uiResolution = UIResolution()
    }

    @Test
    fun uiResolution_ensureItShowsInternetUnavailableErrorViewStateSuccessfully() {
        val resolutionViewState = uiResolution.onInternetUnavailable()

        if (resolutionViewState is ResolutionByCase.ResolutionViewState.InternetUnavailable) {
            assertTrue(true)
        } else {
            fail()
        }
    }

    @Test
    fun uiResolution_ensureItShowsServiceUnavailableErrorViewStateSuccessfully() {
        val resolutionViewState = uiResolution.onHttpException(HttpURLConnection.HTTP_UNAVAILABLE)

        if (resolutionViewState is ResolutionByCase.ResolutionViewState.ServiceUnavailable) {
            assertTrue(true)
        } else {
            fail()
        }
    }

    @Test
    fun uiResolution_ensureItShowsNotFoundErrorViewStateSuccessfully() {
        val resolutionViewState = uiResolution.onHttpException(HttpURLConnection.HTTP_NOT_FOUND)

        if (resolutionViewState is ResolutionByCase.ResolutionViewState.NotFound) {
            assertTrue(true)
        } else {
            fail()
        }
    }

    @Test
    fun uiResolution_ensureItShowsInternalServerErrorViewStateSuccessfully() {
        val resolutionViewState = uiResolution.onHttpException(HttpURLConnection.HTTP_INTERNAL_ERROR)

        if (resolutionViewState is ResolutionByCase.ResolutionViewState.InternalServerError) {
            assertTrue(true)
        } else {
            fail()
        }
    }

}