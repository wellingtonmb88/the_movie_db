package com.wellingtonmb88.themoviedb

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class DateFormatterTest {

    @Test
    fun ensureItFormatsDateSuccessfully() = assertEquals("30/12/2017", dateFormatter("2017-12-30"))

    @Test
    fun ensureItFormatsDateUnSuccessfully() = assertEquals("-", dateFormatter("2017-1"))
}