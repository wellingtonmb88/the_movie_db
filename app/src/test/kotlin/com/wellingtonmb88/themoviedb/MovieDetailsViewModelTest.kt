package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.content.Intent
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.presentation.ui.EXTRA_MOVIE_ITEM
import com.wellingtonmb88.themoviedb.presentation.ui.moviedetails.MovieDetailsViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class MovieDetailsViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val intentMock = mock<Intent>()

    private lateinit var viewModel: MovieDetailsViewModel

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulersForTests()
        viewModel = MovieDetailsViewModel()
    }

    @Test
    fun viewModelLoadMovieAction_ensureItGetsResultSuccessfully() {

        val observer = mock<Observer<MovieModel>>()
        viewModel.movieLiveData.observeForever(observer)
        val mockModel = getMockModel()

        whenever(intentMock.hasExtra(EXTRA_MOVIE_ITEM)).thenReturn(true)

        whenever(intentMock.getParcelableExtra<MovieModel>(EXTRA_MOVIE_ITEM)).thenReturn(mockModel)

        viewModel.loadMovie(intentMock)

        verify(observer).onChanged(mockModel)
    }

    @Test
    fun viewModelLoadMovieAction_ensureItGetsErrorResultSuccessfully() {

        val observerMovieModel = mock<Observer<MovieModel>>()
        viewModel.movieLiveData.observeForever(observerMovieModel)

        val observerBoolean = mock<Observer<Boolean>>()
        viewModel.showErrorLiveData.observeForever(observerBoolean)

        whenever(intentMock.hasExtra(EXTRA_MOVIE_ITEM)).thenReturn(false)

        viewModel.loadMovie(intentMock)

        verifyZeroInteractions(observerMovieModel)

        verify(observerBoolean).onChanged(true)
    }

    private fun getMockModel(): MovieModel {
        return MovieModel(id = 1, title = "title", originalTitle = "", overview = "",
                releaseDate = "", adult = false, posterPath = "", backdropPath = "",
                voteAverage = 5.0, genres = listOf())
    }
}