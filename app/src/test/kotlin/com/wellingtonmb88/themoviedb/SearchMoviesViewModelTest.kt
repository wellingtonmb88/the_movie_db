package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.*
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.resolution.ResolutionByCase
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesViewState
import com.wellingtonmb88.themoviedb.presentation.resolution.UIResolution
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesViewModel
import io.reactivex.Flowable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class SearchMoviesViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val SEARCH_STRING = "SKATE"

    private var interactorMock = mock<SearchMoviesInteractor>()

    private var uiResolutionMock = mock<UIResolution>()

    private lateinit var viewModel: SearchMoviesViewModel

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulersForTests()
        viewModel = SearchMoviesViewModel(interactorMock, uiResolutionMock)
    }

    //Load Search Movies Action

    @Test
    fun viewModelSearchMoviesAction_ensureItGetsEmptyResultSuccessfully() {
        val emptyResultViewState = SearchMoviesViewState.EmptyResult()

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(emptyResultViewState))

        val observer = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(observer)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(observer).onChanged(listOf())

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(emptyResultViewState)
    }

    @Test
    fun viewModelSearchMoviesAction_ensureItGetsResultSuccessfully() {
        val mockListModel = getMockListModel()

        val resultViewState = SearchMoviesViewState.Result(mockListModel)

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(resultViewState))

        val observer = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(observer)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(observer).onChanged(mockListModel)

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(resultViewState)
    }

    @Test
    fun viewModelSearchMoviesAction_ensureItShowsLoadingSuccessfully() {

        val loadingViewState = SearchMoviesViewState.Loading()

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(loadingViewState))

        val isLoadingObserver = mock<Observer<Boolean>>()
        viewModel.isLoadingLiveData.observeForever(isLoadingObserver)

        val observer = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(observer)

        verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(isLoadingObserver).onChanged(true)

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(loadingViewState)

    }

    @Test
    fun viewModelSearchMoviesAction_ensureItShowsErrorSuccessfully() {

        val errorViewState = SearchMoviesViewState.Error(Throwable("Generic Error!"))

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(errorViewState))

        val showErrorSearchObserver = mock<Observer<Boolean>>()
        viewModel.showErrorSearchMoviesLiveData.observeForever(showErrorSearchObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(showErrorSearchObserver).onChanged(true)

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(errorViewState)

    }

    @Test
    fun viewModelSearchMoviesAction_ensureItShowsInternalServerResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_INTERNAL_ERROR
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError500 = Response.error<Any>(errorCode, responseBody)

        val internalServerError = ResolutionByCase.ResolutionViewState.InternalServerError()
        val errorViewState = SearchMoviesViewState.Error(HttpException(responseError500))

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onHttpException(errorCode)).thenReturn(internalServerError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(internalServerError)

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(errorViewState)

    }

    @Test
    fun viewModelSearchMoviesAction_ensureItShowsServiceUnavailableResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_UNAVAILABLE
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val serviceUnavailableError = ResolutionByCase.ResolutionViewState.ServiceUnavailable()
        val errorViewState = SearchMoviesViewState.Error(HttpException(responseError))

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onHttpException(errorCode)).thenReturn(serviceUnavailableError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(serviceUnavailableError)

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(errorViewState)
    }

    @Test
    fun viewModelSearchMoviesAction_ensureItShowsNotFoundResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_NOT_FOUND
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val notFoundError = ResolutionByCase.ResolutionViewState.NotFound()
        val errorViewState = SearchMoviesViewState.Error(HttpException(responseError))

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onHttpException(errorCode)).thenReturn(notFoundError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(notFoundError)

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(errorViewState)
    }

    @Test
    fun viewModelSearchMoviesAction_ensureItShowsInternetUnavailableResolutionErrorSuccessfully() {

        val internetUnavailableError = ResolutionByCase.ResolutionViewState.InternetUnavailable()
        val errorViewState = SearchMoviesViewState.Error(IOException())

        whenever(interactorMock.searchMovies(SEARCH_STRING)).thenReturn(Flowable.just(errorViewState))
        whenever(uiResolutionMock.onInternetUnavailable()).thenReturn(internetUnavailableError)

        val showResolutionErrorObserver = mock<Observer<ResolutionByCase.ResolutionViewState>>()
        viewModel.showResolutionErrorLiveData.observeForever(showResolutionErrorObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.searchMovies(SEARCH_STRING)

        verify(interactorMock).searchMovies(SEARCH_STRING)

        verify(showResolutionErrorObserver, atLeastOnce()).onChanged(internetUnavailableError)

        val testObserverViewState = interactorMock.searchMovies(SEARCH_STRING).test()
        testObserverViewState.assertValue(errorViewState)
    }

    //Load Next Page

    @Test
    fun viewModelLoadNextPageAction_ensureItGetsEmptyResultSuccessfully() {
        val emptyResultViewState = SearchMoviesViewState.EmptyResultNextPage()

        whenever(interactorMock.loadNextPage(SEARCH_STRING)).thenReturn(Flowable.just(emptyResultViewState))

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage(SEARCH_STRING)

        verify(interactorMock).loadNextPage(SEARCH_STRING)

        verify(moviesListObserver).onChanged(listOf())

        val testObserverViewState = interactorMock.loadNextPage(SEARCH_STRING).test()
        testObserverViewState.assertValue(emptyResultViewState)
    }

    @Test
    fun viewModelLoadNextPageAction_ensureItGetsResultSuccessfully() {

        val arrayListOfModel = getMockListModel()
        val resultViewState = SearchMoviesViewState.ResultNextPage(arrayListOfModel)

        whenever(interactorMock.loadNextPage(SEARCH_STRING)).thenReturn(Flowable.just(resultViewState))

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage(SEARCH_STRING)

        verify(interactorMock).loadNextPage(SEARCH_STRING)

        verify(moviesListObserver).onChanged(arrayListOfModel)

        val testObserverViewState = interactorMock.loadNextPage(SEARCH_STRING).test()
        testObserverViewState.assertValue(resultViewState)
    }

    @Test
    fun viewModelLoadNextPageAction_ensureItShowsLoadingSuccessfully() {

        val loadingViewState = SearchMoviesViewState.LoadingNextPage()

        whenever(interactorMock.loadNextPage(SEARCH_STRING)).thenReturn(Flowable.just(loadingViewState))

        val isLoadingObserver = mock<Observer<Boolean>>()
        viewModel.isLoadingNextPageLiveData.observeForever(isLoadingObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage(SEARCH_STRING)

        verify(interactorMock).loadNextPage(SEARCH_STRING)

        verify(isLoadingObserver).onChanged(true)

        val testObserverViewState = interactorMock.loadNextPage(SEARCH_STRING).test()
        testObserverViewState.assertValue(loadingViewState)
    }

    @Test
    fun viewModelLoadNextPageAction_ensureItShowsErrorSuccessfully() {

        val loadingViewState = SearchMoviesViewState.ErrorNexPage(Throwable("Generic Error!"))

        whenever(interactorMock.loadNextPage(SEARCH_STRING)).thenReturn(Flowable.just(loadingViewState))

        val showErrorSearchObserver = mock<Observer<Boolean>>()
        viewModel.showErrorLoadingNexPageLiveData.observeForever(showErrorSearchObserver)

        val moviesListObserver = mock<Observer<List<MovieModel>>>()
        viewModel.moviesListNexPageLiveData.observeForever(moviesListObserver)

        Mockito.verifyNoMoreInteractions(interactorMock)

        viewModel.loadNexPage(SEARCH_STRING)

        verify(interactorMock).loadNextPage(SEARCH_STRING)

        verify(showErrorSearchObserver).onChanged(true)

        val testObserverViewState = interactorMock.loadNextPage(SEARCH_STRING).test()
        testObserverViewState.assertValue(loadingViewState)
    }

    private fun getMockListModel(): List<MovieModel> {
        val MovieModel = MovieModel(id = 1, title = "title", originalTitle = "", overview = "",
                releaseDate = "", adult = false, posterPath = "", backdropPath = "",
                voteAverage = 5.0, genres = listOf())
        return arrayListOf(MovieModel, MovieModel, MovieModel, MovieModel)
    }
}