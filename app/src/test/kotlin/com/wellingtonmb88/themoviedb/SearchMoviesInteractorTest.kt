package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesInteractor
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesLoader
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesViewState
import io.reactivex.Flowable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.HttpException
import retrofit2.Response
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class SearchMoviesInteractorTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val SEARCH_STRING = "SKATE"

    private val EMPTY_STRING = ""

    private var searchMoviesLoaderMock = mock<SearchMoviesLoader>()

    private lateinit var interactor: SearchMoviesInteractor

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulersForTests()
        interactor = SearchMoviesInteractor(searchMoviesLoaderMock)
    }

    //Load Search Movies Action

    @Test
    fun searchMoviesAction_ensureItGetsNotStartedStateSuccessfully() {

        whenever(searchMoviesLoaderMock.loadFirstPage(EMPTY_STRING)).thenReturn(Flowable.just(listOf()))

        interactor.searchMovies(EMPTY_STRING)

        verifyNoMoreInteractions(searchMoviesLoaderMock)

        val testSubscriber = interactor.searchMovies(EMPTY_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(1)
            val viewState = testSubscriber.values()[0]
            when (viewState) {
                is SearchMoviesViewState.NotStarted -> {
                    Assert.assertTrue(true)
                }
                else -> {
                    Assert.assertFalse(true)
                }
            }
        }
    }

    @Test
    fun loadNexPageAction_ensureItGetsNotStartedStateSuccessfully() {
        whenever(searchMoviesLoaderMock.loadNextPage(EMPTY_STRING)).thenReturn(Flowable.just(listOf()))

        interactor.loadNextPage(EMPTY_STRING)

        verifyNoMoreInteractions(searchMoviesLoaderMock)

        val testSubscriber = interactor.loadNextPage(EMPTY_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(1)
            val viewState = testSubscriber.values()[0]
            when (viewState) {
                is SearchMoviesViewState.NotStarted -> {
                    Assert.assertTrue(true)
                }
                else -> {
                    Assert.assertFalse(true)
                }
            }
        }
    }

    @Test
    fun searchMoviesAction_ensureItGetsEmptyResultSuccessfully() {

        val mockListModel: List<MovieModel> = listOf()

        whenever(searchMoviesLoaderMock.loadFirstPage(SEARCH_STRING)).thenReturn(Flowable.just(mockListModel))

        interactor.searchMovies(SEARCH_STRING)

        verify(searchMoviesLoaderMock).loadFirstPage(SEARCH_STRING)

        val testSubscriber = interactor.searchMovies(SEARCH_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is SearchMoviesViewState.Loading -> {
                        Assert.assertTrue(true)
                    }
                    is SearchMoviesViewState.EmptyResult -> {

                        Assert.assertEquals(viewState.toString(), "EmptyResult")
                    }
                    else -> {
                        Assert.fail()
                    }
                }
            }
        }
    }

    @Test
    fun searchMoviesAction_ensureItGetsResultSuccessfully() {

        val mockListModel = getMockListModel()

        whenever(searchMoviesLoaderMock.loadFirstPage(SEARCH_STRING)).thenReturn(Flowable.just(mockListModel))

        interactor.searchMovies(SEARCH_STRING)

        verify(searchMoviesLoaderMock).loadFirstPage(SEARCH_STRING)

        val testSubscriber = interactor.searchMovies(SEARCH_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is SearchMoviesViewState.Loading -> {
                        Assert.assertTrue(true)
                    }
                    is SearchMoviesViewState.Result -> {
                        Assert.assertEquals(mockListModel, viewState.result)

                        Assert.assertEquals(viewState.toString(), "Result{ $mockListModel }")
                    }
                    else -> {
                        Assert.fail()
                    }
                }
            }
        }

    }

    @Test
    fun searchMoviesAction_ensureItShowsServiceUnavailableResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_UNAVAILABLE
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val httpException = HttpException(responseError)

        whenever(searchMoviesLoaderMock.loadFirstPage(SEARCH_STRING)).thenReturn(Flowable.error(httpException))

        interactor.searchMovies(SEARCH_STRING)

        verify(searchMoviesLoaderMock).loadFirstPage(SEARCH_STRING)

        val testSubscriber = interactor.searchMovies(SEARCH_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is SearchMoviesViewState.Loading -> {
                        Assert.assertTrue(true)
                    }
                    is SearchMoviesViewState.Error -> {
                        Assert.assertEquals(httpException, viewState.error)
                        Assert.assertEquals(viewState.toString(), "Error{ $httpException }")
                    }
                    else -> {
                        Assert.fail()
                    }
                }
            }
        }
    }

    //Load Next Page

    @Test
    fun loadNexPageAction_ensureItGetsEmptyResultSuccessfully() {

        val mockListModel: List<MovieModel> = listOf()

        whenever(searchMoviesLoaderMock.loadNextPage(SEARCH_STRING)).thenReturn(Flowable.just(mockListModel))

        interactor.loadNextPage(SEARCH_STRING)

        verify(searchMoviesLoaderMock).loadNextPage(SEARCH_STRING)

        val testSubscriber = interactor.loadNextPage(SEARCH_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is SearchMoviesViewState.LoadingNextPage -> {
                        Assert.assertTrue(true)
                    }
                    is SearchMoviesViewState.EmptyResultNextPage -> {

                        Assert.assertEquals(viewState.toString(), "EmptyResultNextPage")
                    }
                    else -> {
                        Assert.fail()
                    }
                }
            }
        }
    }

    @Test
    fun loadNexPageAction_ensureItGetsResultSuccessfully() {

        val mockListModel = getMockListModel()

        whenever(searchMoviesLoaderMock.loadNextPage(SEARCH_STRING)).thenReturn(Flowable.just(mockListModel))

        interactor.loadNextPage(SEARCH_STRING)

        verify(searchMoviesLoaderMock).loadNextPage(SEARCH_STRING)

        val testSubscriber = interactor.loadNextPage(SEARCH_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is SearchMoviesViewState.LoadingNextPage -> {
                        Assert.assertTrue(true)
                    }
                    is SearchMoviesViewState.ResultNextPage -> {
                        Assert.assertEquals(mockListModel, viewState.result)
                        Assert.assertEquals(viewState.toString(), "ResultNextPage{ $mockListModel }")
                    }
                    else -> {
                        Assert.fail()
                    }
                }
            }
        }
    }

    @Test
    fun loadNexPageAction_ensureItShowsServiceUnavailableResolutionErrorSuccessfully() {

        val errorCode = HttpURLConnection.HTTP_UNAVAILABLE
        val responseBody = ResponseBody.create(MediaType.parse(""), "")
        val responseError = Response.error<Any>(errorCode, responseBody)

        val httpException = HttpException(responseError)

        whenever(searchMoviesLoaderMock.loadNextPage(SEARCH_STRING)).thenReturn(Flowable.error(httpException))

        interactor.loadNextPage(SEARCH_STRING)

        verify(searchMoviesLoaderMock).loadNextPage(SEARCH_STRING)

        val testSubscriber = interactor.loadNextPage(SEARCH_STRING).test()

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.let {
            testSubscriber.assertValueCount(2)

            for (viewState in testSubscriber.values()) {
                when (viewState) {
                    is SearchMoviesViewState.LoadingNextPage -> {
                        Assert.assertTrue(true)
                    }
                    is SearchMoviesViewState.ErrorNexPage -> {
                        Assert.assertEquals(httpException, viewState.error)
                        Assert.assertEquals(viewState.toString(), "ErrorNexPage{ $httpException }")
                    }
                    else -> {
                        Assert.fail()
                    }
                }
            }
        }
    }

    private fun getMockListModel(): List<MovieModel> {
        val MovieModel = MovieModel(id = 1, title = "title", originalTitle = "", overview = "",
                releaseDate = "", adult = false, posterPath = "", backdropPath = "",
                voteAverage = 5.0, genres = listOf())
        return arrayListOf(MovieModel, MovieModel, MovieModel, MovieModel)
    }
}