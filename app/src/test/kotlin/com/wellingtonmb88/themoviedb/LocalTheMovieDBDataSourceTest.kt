package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.wellingtonmb88.themoviedb.data.LocalTheMovieDBDataSource
import com.wellingtonmb88.themoviedb.data.db.TheMovieDBDao
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.model.NowPlayingMoviesResultModel
import com.wellingtonmb88.themoviedb.data.model.mapper.MovieMapper
import com.wellingtonmb88.themoviedb.data.remote.model.MoviesModelApi
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class LocalTheMovieDBDataSourceTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val theMovieDBDaoMock = mock<TheMovieDBDao>()

    private lateinit var localTheMovieDBDataSource: LocalTheMovieDBDataSource

    @Before
    fun setUp() {
        setUpRxSchedulersForTests()
        localTheMovieDBDataSource = LocalTheMovieDBDataSource(theMovieDBDaoMock)
    }

    @Test
    fun getNowPlayingMoviesListAction_ensureItGetsResultSuccessfully() {

        val currentPage = 1
        val mockMoviesModelApi: MoviesModelApi = getMockMoviesModelApi()
        val mockListMovieModel = getMockListMovieModel(mockMoviesModelApi)

        whenever(theMovieDBDaoMock.loadAllNowPlayingMoviesResults(currentPage))
                .thenReturn(Single.just(getMockNowPlayingMoviesResultModel(currentPage, mockMoviesModelApi)))

        whenever(theMovieDBDaoMock.loadAllMovieModelsByIds(mockMoviesModelApi.getMovieIds()))
                .thenReturn(Flowable.just(mockListMovieModel))

        val testSubscriber = localTheMovieDBDataSource.getNowPlayingMoviesList(currentPage).test()

        testSubscriber?.assertValueCount(1)

        testSubscriber?.assertValue(mockListMovieModel)
    }

    @Test
    fun saveNowPlayingMoviesAction_ensureItGetsResultSuccessfully() {

        val currentPage = 1
        val mockMoviesModelApi: MoviesModelApi = getMockMoviesModelApi()
        val mockListMovieModel = getMockListMovieModel(mockMoviesModelApi)

        localTheMovieDBDataSource
                .saveNowPlayingMoviesList(
                        currentPage,
                        mockMoviesModelApi.getMovieIds(),
                        mockListMovieModel)

        verify(theMovieDBDaoMock).insertMovieModels(mockListMovieModel)

        verify(theMovieDBDaoMock)
                .insertNowPlayingMoviesResultModel(
                        NowPlayingMoviesResultModel(currentPage, mockMoviesModelApi.getMovieIds()))
    }

    private fun getMockListMovieModel(mockModel: MoviesModelApi): List<MovieModel> =
            MovieMapper().transform(mockModel.moviesList)

    private fun getMockMoviesModelApi(): MoviesModelApi {
        val responseBody = this.getJsonFile("assets/now_playing_movies.json")
        return Gson().fromJson(responseBody, MoviesModelApi::class.java)
    }

    private fun getMockNowPlayingMoviesResultModel(pageNumber: Int,
                                                   mockModel: MoviesModelApi): NowPlayingMoviesResultModel =
            NowPlayingMoviesResultModel(pageNumber, mockModel.getMovieIds())
}