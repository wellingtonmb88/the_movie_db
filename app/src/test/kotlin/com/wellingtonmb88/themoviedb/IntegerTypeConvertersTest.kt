package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.wellingtonmb88.themoviedb.data.model.IntegerTypeConverters
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class IntegerTypeConvertersTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val STRING = "19,19,19"
    private val INT_LIST = listOf(19, 19, 19)

    private lateinit var integerTypeConverters: IntegerTypeConverters

    @Before
    @Throws(Exception::class)
    fun setUp() {
        integerTypeConverters = IntegerTypeConverters()
    }

    @Test
    fun integerTypeConverters_ensureItConvertsStringToIntListSuccessfully() {
        val result = integerTypeConverters.stringToIntList(STRING)

        assertEquals(INT_LIST, result)
    }

    @Test
    fun integerTypeConverters_ensureItConvertsIntListToStringSuccessfully() {
        val result = integerTypeConverters.intListToString(INT_LIST)

        assertEquals(STRING, result)
    }
}