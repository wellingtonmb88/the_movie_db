package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.wellingtonmb88.themoviedb.data.LocalTheMovieDBDataSource
import com.wellingtonmb88.themoviedb.data.RemoteTheMovieDBDataSource
import com.wellingtonmb88.themoviedb.data.TheMovieDBRepository
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class TheMovieDBRepositoryTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val localTheMovieDBDataSourceMock = mock<LocalTheMovieDBDataSource>()
    private var remoteTheMovieDBDataSourceMock = mock<RemoteTheMovieDBDataSource>()

    private lateinit var theMovieDBRepository: TheMovieDBRepository

    @Before
    fun setUp() {
        setUpRxSchedulersForTests()
        theMovieDBRepository = TheMovieDBRepository(remoteTheMovieDBDataSourceMock, localTheMovieDBDataSourceMock)
    }

    @Test
    fun getNowPlayingMoviesList_ensureItGetsResultSuccessfullyFromLocal() {

        val currentPage = 1
        val mockListModel = getMockListModel()

        whenever(localTheMovieDBDataSourceMock.getNowPlayingMoviesList(currentPage))
                .thenReturn(Flowable.just(mockListModel))

        whenever(remoteTheMovieDBDataSourceMock.getNowPlayingMoviesList(currentPage))
                .thenReturn(Flowable.empty())

        val testSubscriber = theMovieDBRepository.getNowPlayingMoviesList(currentPage).test()

        verify(localTheMovieDBDataSourceMock).getNowPlayingMoviesList(currentPage)

        verifyZeroInteractions(remoteTheMovieDBDataSourceMock)

        testSubscriber?.assertValueCount(1)

        testSubscriber?.assertValue(mockListModel)
    }

    @Test
    fun getNowPlayingMoviesList_ensureItGetsResultSuccessfullyFromRemote() {

        val currentPage = 1
        val mockListModel = getMockListModel()

        whenever(localTheMovieDBDataSourceMock.getNowPlayingMoviesList(currentPage))
                .thenReturn(Flowable.empty())

        whenever(remoteTheMovieDBDataSourceMock.getNowPlayingMoviesList(currentPage))
                .thenReturn(Flowable.just(mockListModel))

        val testSubscriber = theMovieDBRepository.getNowPlayingMoviesList(currentPage).test()

        verify(localTheMovieDBDataSourceMock).getNowPlayingMoviesList(currentPage)

        verify(remoteTheMovieDBDataSourceMock).getNowPlayingMoviesList(currentPage)

        testSubscriber?.assertValueCount(1)

        testSubscriber?.assertValue(mockListModel)
    }

    private fun getMockListModel(): List<MovieModel> {
        val MovieModel = MovieModel(id = 1, title = "title", originalTitle = "", overview = "",
                releaseDate = "", adult = false, posterPath = "", backdropPath = "",
                voteAverage = 5.0, genres = listOf())
        return arrayListOf(MovieModel, MovieModel, MovieModel, MovieModel)
    }
}