package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.wellingtonmb88.themoviedb.data.TheMovieDBRepository
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.domain.searchmovies.SearchMoviesLoader
import io.reactivex.Flowable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class SearchMoviesLoaderTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val SEARCH_STRING = "SKATE"

    private var theMovieDBRepositoryMock = mock<TheMovieDBRepository>()

    private lateinit var searchMoviesLoader: SearchMoviesLoader

    @Before
    @Throws(Exception::class)
    fun setUp() {
        setUpRxSchedulersForTests()
        searchMoviesLoader = SearchMoviesLoader(theMovieDBRepositoryMock)
    }

    //Load First Page Action

    @Test
    fun loadFirstPageAction_ensureItGetsEmptyResultSuccessfully() {
        val currentPage = 1
        val mockListModel: List<MovieModel> = listOf()

        whenever(theMovieDBRepositoryMock.searchMoviesList(SEARCH_STRING, currentPage)).thenReturn(Flowable.just(mockListModel))

        val testSubscriber = searchMoviesLoader.loadFirstPage(SEARCH_STRING).test()

        verify(theMovieDBRepositoryMock).searchMoviesList(SEARCH_STRING, currentPage)

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.assertValue(mockListModel)
    }

    @Test
    fun loadFirstPageAction_ensureItGetsResultSuccessfully() {
        val currentPage = 1
        val mockListModel = getMockListModel()

        whenever(theMovieDBRepositoryMock.searchMoviesList(SEARCH_STRING, currentPage)).thenReturn(Flowable.just(mockListModel))

        val testSubscriber = searchMoviesLoader.loadFirstPage(SEARCH_STRING).test()

        verify(theMovieDBRepositoryMock).searchMoviesList(SEARCH_STRING, currentPage)

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.assertValue(mockListModel)
    }

    @Test
    fun loadNextPagePageAction_ensureItGetsEmptyResultSuccessfully() {
        val currentPage = 2
        val mockListModel: List<MovieModel> = listOf()

        whenever(theMovieDBRepositoryMock.searchMoviesList(SEARCH_STRING, currentPage)).thenReturn(Flowable.just(mockListModel))

        val testSubscriber = searchMoviesLoader.loadNextPage(SEARCH_STRING).test()

        verify(theMovieDBRepositoryMock).searchMoviesList(SEARCH_STRING, currentPage)

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.assertValue(mockListModel)
    }

    @Test
    fun loadNextPagePageAction_ensureItGetsResultSuccessfully() {
        val currentPage = 2
        val mockListModel = getMockListModel()

        whenever(theMovieDBRepositoryMock.searchMoviesList(SEARCH_STRING, currentPage)).thenReturn(Flowable.just(mockListModel))

        val testSubscriber = searchMoviesLoader.loadNextPage(SEARCH_STRING).test()

        verify(theMovieDBRepositoryMock).searchMoviesList(SEARCH_STRING, currentPage)

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.assertValue(mockListModel)
    }

    @Test
    fun loadNextPagePageAction_ensureItGetsEmptyResultWhenEndReachedSuccessfully() {
        val currentPage = 2
        val mockListModel: List<MovieModel> = listOf()

        whenever(theMovieDBRepositoryMock.searchMoviesList(SEARCH_STRING, currentPage)).thenReturn(Flowable.just(mockListModel))

        val testSubscriber = searchMoviesLoader.loadNextPage(SEARCH_STRING).test()

        verify(theMovieDBRepositoryMock).searchMoviesList(SEARCH_STRING, currentPage)

        Assert.assertNotNull(testSubscriber)

        testSubscriber?.assertValue(mockListModel)

        searchMoviesLoader.loadNextPage(SEARCH_STRING).test()

        verifyNoMoreInteractions(theMovieDBRepositoryMock)
    }

    private fun getMockListModel(): List<MovieModel> {
        val MovieModel = MovieModel(id = 1, title = "title", originalTitle = "", overview = "",
                releaseDate = "", adult = false, posterPath = "", backdropPath = "",
                voteAverage = 0.5, genres = listOf())
        return arrayListOf(MovieModel, MovieModel, MovieModel, MovieModel)
    }
}