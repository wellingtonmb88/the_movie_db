package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.Gson
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import com.wellingtonmb88.themoviedb.data.LocalTheMovieDBDataSource
import com.wellingtonmb88.themoviedb.data.RemoteTheMovieDBDataSource
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.data.model.mapper.MovieMapper
import com.wellingtonmb88.themoviedb.data.remote.TheMovieDB
import com.wellingtonmb88.themoviedb.data.remote.TheMovieDBApi
import com.wellingtonmb88.themoviedb.data.remote.model.GenresModelApi
import com.wellingtonmb88.themoviedb.data.remote.model.MoviesModelApi
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit

@RunWith(JUnit4::class)
class RemoteTheMovieDBDataSourceTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val LANG = "en-US"

    private val retrofitMock = mock<Retrofit>()

    private val theMovieDBMock = mock<TheMovieDB>()

    private val localTheMovieDBDataSourceMock = mock<LocalTheMovieDBDataSource>()

    private lateinit var remoteTheMovieDBDataSource: RemoteTheMovieDBDataSource

    private lateinit var theMovieDBApi: TheMovieDBApi

    @Before
    fun setUp() {
        setUpRxSchedulersForTests()
        theMovieDBApi = TheMovieDBApi(retrofitMock)
        remoteTheMovieDBDataSource = RemoteTheMovieDBDataSource(theMovieDBApi, localTheMovieDBDataSourceMock)
    }

    @Test
    fun getNowPlayingMoviesListAction_ensureItGetsResultSuccessfully() {

        val currentPage = 1
        val mockMoviesModelApi: MoviesModelApi = getMockMoviesModelApi()
        val mockListMovieModel = getMockListMovieModel(mockMoviesModelApi)

        whenever(retrofitMock.create(TheMovieDB::class.java)).thenReturn(theMovieDBMock)

        whenever(theMovieDBApi.createService().getNowPlayingMoviesList(currentPage.toString(), LANG))
                .thenReturn(Single.just(mockMoviesModelApi))

        whenever(theMovieDBApi.createService().getGenresList(LANG))
                .thenReturn(Single.just(getMockGenresModelApi()))

        val testSubscriber = remoteTheMovieDBDataSource.getNowPlayingMoviesList(currentPage).test()

        testSubscriber?.assertValueCount(1)

        testSubscriber?.assertValue(mockListMovieModel)
    }

    private fun getMockListMovieModel(mockModel: MoviesModelApi): List<MovieModel> =
            MovieMapper().transform(mockModel.moviesList)

    private fun getMockMoviesModelApi(): MoviesModelApi {
        val responseBody = this.getJsonFile("assets/now_playing_movies.json")
        return Gson().fromJson(responseBody, MoviesModelApi::class.java)
    }

    private fun getMockGenresModelApi(): GenresModelApi {
        val responseBody = this.getJsonFile("assets/genres_en_us.json")
        return Gson().fromJson(responseBody, GenresModelApi::class.java)
    }
}