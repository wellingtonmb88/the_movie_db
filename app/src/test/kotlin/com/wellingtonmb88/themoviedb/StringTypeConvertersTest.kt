package com.wellingtonmb88.themoviedb

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.wellingtonmb88.themoviedb.data.model.StringTypeConverters
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class StringTypeConvertersTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val STRING = "NOME1,NOME2,NOME3"
    private val STRING_LIST = arrayListOf("NOME1", "NOME2", "NOME3")

    private lateinit var stringTypeConverters: StringTypeConverters

    @Before
    @Throws(Exception::class)
    fun setUp() {
        stringTypeConverters = StringTypeConverters()
    }

    @Test
    fun stringTypeConverters_ensureItConvertsStringToStringListSuccessfully() {
        val result = stringTypeConverters.stringToStringList(STRING)

        assertEquals(STRING_LIST, result)
    }

    @Test
    fun stringTypeConverters_ensureItConvertsStringListToStringSuccessfully() {
        val result = stringTypeConverters.stringListToString(STRING_LIST)

        assertEquals(STRING, result)
    }
}