package com.wellingtonmb88.themoviedb

import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.matcher.IntentMatchers
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.linkedin.android.testbutler.TestButler
import com.wellingtonmb88.themoviedb.presentation.ui.moviedetails.MovieDetailsActivity
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesActivity
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class SearchMoviesActivityTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(SearchMoviesActivity::class.java, true, false)

    private val SEARCH_STRING = "JAVA"

    private val SLEEP = 1000L

    private fun searchMoviesRobots(func: SearchMoviesRobots.() -> Unit) =
            SearchMoviesRobots(activityRule).apply { func() }

    @After
    fun tearDown() {
        searchMoviesRobots { closeServer() }
    }

    @Test
    fun ensureItShowsEmptyStateSuccessfully() {
        searchMoviesRobots {
            enqueueServerEmptyResultSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkEmptyStateIsVisible()
        }
    }

    @Test
    fun ensureItShowsMoviesListSuccessfully() {
        searchMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

    @Test
    fun ensureItScrollsToEndAndShowNextPageMoviesListResultSuccessfully() {
        searchMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            enqueueServerNextPageSuccessfully()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Next Page")
        }
    }

    @Test
    fun ensureItScrollsToEndAndShowNextPageErrorMessageSuccessfully() {
        searchMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            enqueueInternalServerError()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            checkNextPageErrorMessageVisible()
        }
    }

    @Test
    fun ensureItScrollsToEndAndShowNextPageEmptyResultSuccessfully() {
        searchMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            enqueueServerEmptyResultSuccessfully()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            checkNextPageEmptyResultMessageVisible()
        }
    }

    @Test
    fun ensureItShowsInternalServerErrorMessageSuccessfully() {
        searchMoviesRobots {
            enqueueInternalServerError()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Sorry, unexpected error! Please try again later!")
        }
    }

    @Test
    fun ensureItShowsServiceUnavailableErrorMessageSuccessfully() {
        searchMoviesRobots {
            enqueueServiceUnavailable()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Sorry, server unavailable! Please try again later!")
        }
    }

    @Test
    fun ensureItShowsServiceUnavailableErrorMessageAndTryAgainSuccessfully() {
        searchMoviesRobots {
            enqueueServiceUnavailable()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Sorry, server unavailable! Please try again later!")
            enqueueServerSuccessfully()
            clickTryAgainButton()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

    @Test
    fun ensureItShowsServerNotFoundErrorMessageSuccessfully() {
        searchMoviesRobots {
            enqueueServerNotFound()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Requested server not found!")
        }
    }

    @Test
    fun ensureItShowsInternetUnavailableErrorMessageSuccessfully() {
        TestButler.setWifiState(false)
        TestButler.setGsmState(false)
        searchMoviesRobots {
            enqueueInternetUnavailable()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkInternetUnavailableMessageVisible()
        }
        TestButler.setWifiState(true)
        TestButler.setGsmState(true)
    }

    @Test
    fun ensureItOpensMovieDetailsSuccessfully() {
        searchMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            Intents.init()
            clickRecyclerView(0)
            Intents.intended(IntentMatchers.hasComponent(MovieDetailsActivity::class.java.name))
            Intents.release()
        }
    }

    @Test
    fun ensureItOpensSearchMoviesFromMovieDetailsWithBackUpButtonSuccessfully() {
        searchMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            Intents.init()
            clickRecyclerView(0)
            Intents.intended(IntentMatchers.hasComponent(MovieDetailsActivity::class.java.name))
            Intents.release()
            clickBackUpButton()
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

    @Test
    fun ensureItOpensSearchMoviesFromMovieDetailsWithBackButtonSuccessfully() {
        searchMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            clickOnSearchMenu()
            typeTextOnSearchView(SEARCH_STRING)
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            Intents.init()
            clickRecyclerView(0)
            Intents.intended(IntentMatchers.hasComponent(MovieDetailsActivity::class.java.name))
            Intents.release()
            pressBackButton()
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

}