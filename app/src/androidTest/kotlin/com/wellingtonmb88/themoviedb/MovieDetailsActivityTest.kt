package com.wellingtonmb88.themoviedb

import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4
import com.wellingtonmb88.themoviedb.presentation.ui.moviedetails.MovieDetailsActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class MovieDetailsActivityTest {

    @Rule
    @JvmField
    val activityRule = IntentsTestRule(MovieDetailsActivity::class.java, true, false)

    private val SLEEP = 500L

    private fun movieDetailsRobots(func: MovieDetailsRobots.() -> Unit) =
            MovieDetailsRobots(activityRule).apply { func() }

    @Test
    fun ensureItShowsContentSuccessfully() {
        movieDetailsRobots {
            startActivityWithExtra()
            Thread.sleep(SLEEP)
            checkText("Overview", isDisplayed = true)
            checkText("09/12/1900", isDisplayed = true)
            checkText("Adult: Yes", isDisplayed = true)
            checkText("Genres: Thriller, Drama", isDisplayed = true)
            checkText("5.0", isDisplayed = true)
            scrollToTop()
            scrollToTop()
            scrollToTop()
            checkToolbarTitle("title")
        }
    }

    @Test
    fun ensureItShowErrorSuccessfully() {
        movieDetailsRobots {
            startActivityWithNoExtra()
            Thread.sleep(SLEEP)
            checkContentLayoutIsNotVisible()
            checkrErrorLayoutIsVisible()
        }
    }
}