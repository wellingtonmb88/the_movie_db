package com.wellingtonmb88.themoviedb

import android.support.test.espresso.intent.Intents.*
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.linkedin.android.testbutler.TestButler
import com.wellingtonmb88.themoviedb.data.db.DatabaseCreator
import com.wellingtonmb88.themoviedb.data.db.TheMovieDBDao
import com.wellingtonmb88.themoviedb.presentation.ui.moviedetails.MovieDetailsActivity
import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesActivity
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class NowPlayingMoviesActivityTest {

    @Rule
    @JvmField
    val activityRule = ActivityTestRule(NowPlayingMoviesActivity::class.java, true, false)

    private val SLEEP = 500L

    private lateinit var theMovieDBDao: TheMovieDBDao

    private fun nowPlayingMoviesRobots(func: NowPlayingMoviesRobots.() -> Unit) =
            NowPlayingMoviesRobots(activityRule).apply { func() }

    @Before
    fun setUp() {
        theMovieDBDao = DatabaseCreator.database.theMovieDBDao()
    }

    @After
    fun tearDown() {
        nowPlayingMoviesRobots { closeServer() }
        theMovieDBDao.deleteAllNowPlayingMoviesResults()
        theMovieDBDao.deleteAllMovieModels()
    }

    @Test
    fun ensureItShowsEmptyStateSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerEmptyResultSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkEmptyStateIsVisible()
        }
    }

    @Test
    fun ensureItShowsMoviesListSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

    @Test
    fun ensureItScrollsToEndAndShowNextPageMoviesListResultSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            enqueueServerNextPageSuccessfully()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Next Page")
        }
    }

    @Test
    fun ensureItScrollsToEndAndShowNextPageErrorMessageSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            enqueueInternalServerError()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            checkNextPageErrorMessageVisible()
        }
    }

    @Test
    fun ensureItScrollsToEndAndShowNextPageEmptyResultSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            enqueueServerEmptyResultSuccessfully()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            swipeViewUp()
            Thread.sleep(SLEEP)
            checkNextPageEmptyResultMessageVisible()
        }
    }

    @Test
    fun ensureItShowsInternalServerErrorMessageSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueInternalServerError()
            startActivity()
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Sorry, unexpected error! Please try again later!")
        }
    }

    @Test
    fun ensureItShowsServiceUnavailableErrorMessageSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServiceUnavailable()
            startActivity()
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Sorry, server unavailable! Please try again later!")
        }
    }

    @Test
    fun ensureItShowsServiceUnavailableErrorMessageAndTryAgainSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServiceUnavailable()
            startActivity()
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Sorry, server unavailable! Please try again later!")
            enqueueServerSuccessfully()
            clickTryAgainButton()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

    @Test
    fun ensureItShowsServerNotFoundErrorMessageSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerNotFound()
            startActivity()
            Thread.sleep(SLEEP)
            checkErrorTextMessage("Requested server not found!")
        }
    }

    @Test
    fun ensureItShowsInternetUnavailableErrorMessageSuccessfully() {
        TestButler.setWifiState(false)
        TestButler.setGsmState(false)
        nowPlayingMoviesRobots {
            enqueueInternetUnavailable()
            startActivity()
            Thread.sleep(SLEEP)
            checkInternetUnavailableMessageVisible()
        }
        TestButler.setWifiState(true)
        TestButler.setGsmState(true)
    }

    @Test
    fun ensureItOpensMovieDetailsSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            init()
            clickRecyclerView(0)
            intended(hasComponent(MovieDetailsActivity::class.java.name))
            release()
        }
    }

    @Test
    fun ensureItOpensNowPlayingMoviesFromMovieDetailsWithBackUpButtonSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            init()
            clickRecyclerView(0)
            intended(hasComponent(MovieDetailsActivity::class.java.name))
            release()
            clickBackUpButton()
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

    @Test
    fun ensureItOpensNowPlayingMoviesFromMovieDetailsWithBackButtonSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(1000L)
            checkDescendantRecyclerView("Annabelle: Creation")
            init()
            clickRecyclerView(0)
            intended(hasComponent(MovieDetailsActivity::class.java.name))
            release()
            pressBackButton()
            checkDescendantRecyclerView("Annabelle: Creation")
        }
    }

    @Test
    fun ensureItOpensSearchMoviesSuccessfully() {
        nowPlayingMoviesRobots {
            enqueueServerSuccessfully()
            startActivity()
            Thread.sleep(SLEEP)
            checkDescendantRecyclerView("Annabelle: Creation")
            init()
            clickOnSearchMenu()
            intended(hasComponent(SearchMoviesActivity::class.java.name))
            release()
        }
    }
}