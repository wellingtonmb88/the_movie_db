package com.wellingtonmb88.themoviedb

import android.app.Activity
import android.content.Intent
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import com.wellingtonmb88.themoviedb.presentation.ui.nowplayingmovies.NowPlayingMoviesAdapter
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers
import java.net.HttpURLConnection


class NowPlayingMoviesRobots(private val activityRule: ActivityTestRule<out Activity>) {

    private val server = MockWebServer()

    private fun setupTestServerUrl() {
        TestServerUrl.url = server.url("/")
    }

    fun closeServer() {
        server.close()
        server.shutdown()
    }

    private fun enqueueGenreServerSuccessfully() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/genres_en_us.json")))
    }

    fun enqueueServerSuccessfully() {
        setupTestServerUrl()
        enqueueGenreServerSuccessfully()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/now_playing_movies.json")))

    }

    fun enqueueServerNextPageSuccessfully() {
        setupTestServerUrl()
        enqueueGenreServerSuccessfully()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/now_playing_movies_next_page.json")))

    }

    fun enqueueServerEmptyResultSuccessfully() {
        setupTestServerUrl()
        enqueueGenreServerSuccessfully()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/now_playing_movies_empty.json")))

    }

    fun enqueueInternalServerError() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_INTERNAL_ERROR))
    }

    fun enqueueServiceUnavailable() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_UNAVAILABLE))
    }

    fun enqueueServerNotFound() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_NOT_FOUND))
    }

    fun enqueueInternetUnavailable() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK))
    }

    fun startActivity() =
            activityRule.launchActivity(Intent())!!

    fun clickOnSearchMenu() =
            onView(withId(R.id.action_search)).perform(click())!!

    fun clickTryAgainButton() =
            onView(withId(R.id.try_again_button))
                    .check(matches(isDisplayed())).perform(click())!!

    fun checkErrorTextMessage(text: String) =
            onView(withId(R.id.error_message_text_view))
                    .check(matches(isDisplayed()))
                    .check(matches(withText(text)))!!

    fun checkEmptyStateIsVisible() =
            onView(withId(R.id.empty_state_layout))
                    .check(matches(isDisplayed()))!!

    fun checkDescendantRecyclerView(text: String) =
            onView(withId(R.id.movies_list_rcv))
                    .check(matches(isDisplayed()))
                    .check(matches(hasDescendant(withText(text))))!!

    fun clickRecyclerView(position: Int) =
            onView(withId(R.id.movies_list_rcv))
                    .check(matches(isDisplayed()))
                    .perform(RecyclerViewActions
                            .actionOnItemAtPosition<NowPlayingMoviesAdapter.NowPlayingMoviesViewHolder>(position, click()))!!

    fun clickBackUpButton() =
            onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click())!!

    fun pressBackButton() =
            Espresso.pressBack()

    fun checkInternetUnavailableMessageVisible() =
            onView(Matchers.allOf(withId(
                    android.support.design.R.id.snackbar_text),
                    withText("No Internet Connection Available!")))
                    .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))!!

    fun checkNextPageErrorMessageVisible() =
            onView(Matchers.allOf(withId(
                    android.support.design.R.id.snackbar_text),
                    withText("Error loading more content!")))
                    .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))!!

    fun checkNextPageEmptyResultMessageVisible() =
            onView(Matchers.allOf(withId(
                    android.support.design.R.id.snackbar_text),
                    withText("No more results!")))
                    .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))!!

    fun swipeViewUp() =
            onView(withId(R.id.activity_now_playing_movies_layout))
                    .perform(swipeUp())!!

}
