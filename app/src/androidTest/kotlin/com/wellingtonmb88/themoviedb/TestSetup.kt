package com.wellingtonmb88.themoviedb

import android.app.Application
import android.content.Context
import android.os.Bundle
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.IdlingResource
import android.support.test.runner.AndroidJUnitRunner
import com.linkedin.android.testbutler.TestButler
import com.wellingtonmb88.themoviedb.di.ActivityModule
import com.wellingtonmb88.themoviedb.di.AppComponent
import com.wellingtonmb88.themoviedb.di.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.multibindings.IntoSet
import okhttp3.Dispatcher
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import timber.log.Timber
import javax.inject.Named
import javax.inject.Singleton


class TheMovieDBTestRunner : AndroidJUnitRunner() {

    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application =
            super.newApplication(cl, TestCustomApplication::class.java.name, context)

    override fun onStart() {
        TestButler.setup(InstrumentationRegistry.getTargetContext())
        super.onStart()
    }

    override fun finish(resultCode: Int, results: Bundle) {
        TestButler.teardown(InstrumentationRegistry.getTargetContext())
        super.finish(resultCode, results)
    }
}

class TestCustomApplication : TheMovieDBApplication() {

    override fun createComponent() =
            DaggerTestAppComponent.builder()
                    .application(this)
                    .build()
}

@Singleton
@Component(modules = arrayOf(
        AndroidInjectionModule::class,
        ActivityModule::class,
        AppModule::class,
        TestNetworkUtilitiesModule::class
))
interface TestAppComponent : AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}

object TestServerUrl {
    var url: HttpUrl? = null
}

@Module
class TestNetworkUtilitiesModule {

    /* Force HTTP scheme for testing */
    @Provides
    @Singleton
    @Named("HTTP_URL")
    fun providesHttpUrl() = "http://localhost/"

    @Provides
    @IntoSet
    fun providesOkHttpClient(builder: OkHttpClient.Builder): OkHttpClient {
        val client = builder.build()
        IdlingRegistry.getInstance().register(OkHttp3IdlingResource("TheMovieDB", client.dispatcher()))
        return client
    }

    @Provides
    @IntoSet
    fun providesMockWebServerInterceptor(): Interceptor {
        return Interceptor {

            var request = it.request()

            Timber.w("HTTP_URL_IN", request.url().toString())

            TestServerUrl.url?.let {
                request = request.newBuilder()
                        .url(request.url()
                                .newBuilder()
                                .host(TestServerUrl.url!!.host())
                                .port(TestServerUrl.url!!.port())
                                .build())
                        .build()
            }

            Timber.w("HTTP_URL_OUT", request.url().toString())

            it.proceed(request)
        }
    }
}

class OkHttp3IdlingResource(private val resourceName: String, private val dispatcher: Dispatcher) : IdlingResource {

    @Volatile internal var callback: IdlingResource.ResourceCallback? = null

    init {
        dispatcher.setIdleCallback {
            val callback = this@OkHttp3IdlingResource.callback
            callback?.onTransitionToIdle()
        }
    }

    override fun getName() = resourceName
    override fun isIdleNow() = dispatcher.runningCallsCount() == 0

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        this.callback = callback
    }
}
