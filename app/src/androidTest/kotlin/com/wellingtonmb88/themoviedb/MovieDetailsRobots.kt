package com.wellingtonmb88.themoviedb

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.swipeUp
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.intent.Intents.*
import android.support.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.BoundedMatcher
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.v7.widget.Toolbar
import com.wellingtonmb88.themoviedb.data.model.MovieModel
import com.wellingtonmb88.themoviedb.presentation.ui.EXTRA_MOVIE_ITEM
import com.wellingtonmb88.themoviedb.presentation.ui.moviedetails.MovieDetailsActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.not
import org.hamcrest.core.Is


class MovieDetailsRobots(private val activityRule: ActivityTestRule<out Activity>) {

    fun startActivityWithExtra() {
        init()
        val resultData = Intent()
        val mockModel = getMockModel()

        resultData.putExtra(EXTRA_MOVIE_ITEM, mockModel)

        val result = Instrumentation.ActivityResult(Activity.RESULT_OK, resultData)

        intending(hasComponent(hasClassName(MovieDetailsActivity::class.java.name)))
                .respondWith(result)
        release()

        activityRule.launchActivity(resultData)
    }

    fun startActivityWithNoExtra() {
        activityRule.launchActivity(Intent())
    }

    fun checkContentLayoutIsNotVisible() =
            onView(withId(R.id.movie_details_layout))
                    .check(matches(not(isDisplayed())))!!

    fun checkrErrorLayoutIsVisible() =
            onView(withId(R.id.error_message_layout))
                    .check(matches(isDisplayed()))!!

    fun checkText(text: String, isDisplayed: Boolean) =
            if (isDisplayed) {
                onView(withId(R.id.activity_movie_details_layout))
                        .check(matches(hasDescendant(withText(text))))!!
            } else {
                onView(withId(R.id.activity_movie_details_layout))
                        .check(matches(hasDescendant(withText(text))))
                        .check(matches(not(isDisplayed())))!!
            }

    fun scrollToTop() =
            onView(withId(R.id.movie_details_layout))
                    .perform(swipeUp())!!

    fun checkToolbarTitle(title: String) =
            onView(isAssignableFrom(Toolbar::class.java))
                    .check(matches(withToolbarTitle(Is.`is`(title))))!!


    private fun withToolbarTitle(textMatcher: Matcher<CharSequence>): Matcher<Any> {
        return object : BoundedMatcher<Any, Toolbar>(Toolbar::class.java) {
            public override fun matchesSafely(toolbar: Toolbar): Boolean =
                    textMatcher.matches(toolbar.title)

            override fun describeTo(description: Description) {
                description.appendText("with toolbar title: ")
                textMatcher.describeTo(description)
            }
        }
    }

    private fun getMockModel(): MovieModel {
        return MovieModel(id = 1, title = "title", originalTitle = "", overview = "Overview",
                releaseDate = "1900-12-09", adult = true, posterPath = "", backdropPath = "",
                voteAverage = 5.0, genres = listOf("Thriller", "Drama"))
    }
}