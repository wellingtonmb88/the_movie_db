package com.wellingtonmb88.themoviedb

import android.app.Activity
import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import com.wellingtonmb88.themoviedb.presentation.ui.searchmovies.SearchMoviesAdapter
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.Matchers
import java.net.HttpURLConnection

class SearchMoviesRobots(private val activityRule: ActivityTestRule<out Activity>) {

    private val server = MockWebServer()

    private fun setupTestServerUrl() {
        TestServerUrl.url = server.url("/")
    }

    fun closeServer() {
        server.close()
        server.shutdown()
    }

    private fun enqueueGenreServerSuccessfully() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/genres_en_us.json")))
    }

    fun enqueueServerSuccessfully() {
        setupTestServerUrl()
        enqueueGenreServerSuccessfully()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/now_playing_movies.json")))

    }

    fun enqueueServerNextPageSuccessfully() {
        setupTestServerUrl()
        enqueueGenreServerSuccessfully()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/now_playing_movies_next_page.json")))

    }

    fun enqueueServerEmptyResultSuccessfully() {
        setupTestServerUrl()
        enqueueGenreServerSuccessfully()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(this.getJsonFile("assets/now_playing_movies_empty.json")))

    }

    fun enqueueInternalServerError() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_INTERNAL_ERROR))
    }

    fun enqueueServiceUnavailable() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_UNAVAILABLE))
    }

    fun enqueueServerNotFound() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_NOT_FOUND))
    }

    fun enqueueInternetUnavailable() {
        setupTestServerUrl()
        server.enqueue(MockResponse().setResponseCode(HttpURLConnection.HTTP_OK))
    }

    fun startActivity() =
            activityRule.launchActivity(Intent())!!

    fun clickOnSearchMenu() =
            onView(ViewMatchers.withId(R.id.action_search)).perform(ViewActions.click())!!

    fun typeTextOnSearchView(text: String) =
            onView(ViewMatchers.withId(android.support.design.R.id.search_src_text))
                    .perform(ViewActions.typeText(text), ViewActions.closeSoftKeyboard())!!

    fun clickTryAgainButton() =
            onView(ViewMatchers.withId(R.id.try_again_button))
                    .check(ViewAssertions.matches(ViewMatchers.isDisplayed())).perform(ViewActions.click())!!

    fun checkErrorTextMessage(text: String) =
            onView(ViewMatchers.withId(R.id.error_message_text_view))
                    .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.withText(text)))!!

    fun checkEmptyStateIsVisible() =
            onView(ViewMatchers.withId(R.id.empty_state_layout))
                    .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))!!

    fun checkDescendantRecyclerView(text: String) =
            onView(ViewMatchers.withId(R.id.movies_list_rcv))
                    .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
                    .check(ViewAssertions.matches(ViewMatchers.hasDescendant(ViewMatchers.withText(text))))!!

    fun clickRecyclerView(position: Int) =
            onView(ViewMatchers.withId(R.id.movies_list_rcv))
                    .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
                    .perform(RecyclerViewActions.actionOnItemAtPosition<SearchMoviesAdapter.SearchMoviesViewHolder>(position, ViewActions.click()))!!

    fun clickBackUpButton() =
            onView(ViewMatchers.withContentDescription(R.string.abc_action_bar_up_description)).perform(ViewActions.click())!!

    fun pressBackButton() =
            pressBack()

    fun checkInternetUnavailableMessageVisible() =
            onView(Matchers.allOf(ViewMatchers.withId(
                    android.support.design.R.id.snackbar_text),
                    ViewMatchers.withText("No Internet Connection Available!")))
                    .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))!!

    fun checkNextPageErrorMessageVisible() =
            onView(Matchers.allOf(ViewMatchers.withId(
                    android.support.design.R.id.snackbar_text),
                    ViewMatchers.withText("Error loading more content!")))
                    .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))!!

    fun checkNextPageEmptyResultMessageVisible() =
            onView(Matchers.allOf(ViewMatchers.withId(
                    android.support.design.R.id.snackbar_text),
                    ViewMatchers.withText("No more results!")))
                    .check(ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)))!!

    fun swipeViewUp() =
            onView(ViewMatchers.withId(R.id.activity_now_playing_movies_layout))
                    .perform(ViewActions.swipeUp())!!

}